use knjizara;
insert into knjiga(ISBN,name,izdavackaKuca,autor,releaseYear,description,picture,
price,pageCount,hard,latinica,language,avgGrade,brojKnjiga)
values(1234567891011,'1984','Laguna','George Orwel',
'1980-8-7','Dobra knjiga',
'https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1532714506l/40961427._SX318_.jpg',1500,500,0,1,
'English', 0,5),(2234567891012,'Knjiga 2','Laguna','George Orwel',
'1999-10-4','Losa knjiga',
'https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1532714506l/40961427._SX318_.jpg',1500,500,0,1,
'English', 0,1),(7894561234567,'Knjiga 3','Vulkan','Virgil',
'1895-10-8','Knjiga je dobra',
'https://images-na.ssl-images-amazon.com/images/I/51284cxPAzL._SX333_BO1,204,203,200_.jpg',500,143,0,0,
'English', 0,45),(2894561234567,'The Aneid','Laguna','Virgil',
'1835-9-8','Knjiga je onakva',
'https://images-na.ssl-images-amazon.com/images/I/51284cxPAzL._SX333_BO1,204,203,200_.jpg',500,143,0,0,
'English', 0,45),(7531598426105,'Blindnes','Vulkan','Saramago',
'1745-4-7','Knjiga je losa',
'https://cdn.waterstones.com/bookjackets/large/9780/0995/9780099573586.jpg',7894,56,0,1,
'English',0,20),(4831598426105,'Blindnes','Vulkan','Saramago',
'1790-5-10','Knjiga je losa',
'https://cdn.waterstones.com/bookjackets/large/9780/0995/9780099573586.jpg',7199,256,1,1,
'English',0,150);


insert into korisnik(username,password,email,firstname,lastname,birthDate,
adress,phonenumber,admin,izblokiran)
values('jovan22','prpa123','prpajovan22@gmail.com','Jovan','Prpa',
'2020-11-11','Kralja Milana 10','0600424221',1,0),('milan11','prpa234','milanprpa123@gmail.com','Milan','Prpa',
'1984-12-12','Kralja Milana 10','0600424481',0,1),('milos45','milosevic89','milanmilic11@gmail.com','Milos','Milosevic',
'2020-7-5','Kralja Petra 5','0216394539',0,0),('admin','admin1','admin@gmail.com','Admin','Admin',
'2020-8-1','Kralja Petra 6','0216378539',1,0),('korisnik','korisnik','korisnik@gmail.com','korisnik','korisnik',
'2020-4-3','Ulica','0216378539',0,0);


insert into zanrovi(id,name,description)
values(13,'Si-Fi','Ovo je opis zanra za si-fi'),('14','Istorija','Dogadjaji koji su se desili u istoriji');

insert into komentar(text,grade,date,korisnikId,knjigaISBN,status)
values ('Knjiga je ok','4','2020-11-9',2,'1234567891011','approved'),('Mogla je biti bolja','3','2020-11-11',1,'1234567891011','waiting'),
('Losa knjiga','1','2020-7-11',1,'1234567891011','approved'),('Veoma mi se svidela knjiga','5','1999-6-8','2',7531598426105,'approved');

insert into kartica(id,discount,pointCount,korisnikId,statusKartice)
values(1,20,4,5,1);

insert into izvestaj(id,prodataKnjiga,datumKupovine,brojProdatijh,cenaProdatih)
value(1,1234567891011,'2020-09-04',10,15000),(2,1234567891011,'2020-09-07',1,1500);

insert into SpecialDate(id,specialDate,popust)
values(1,'2020-09-07',500)