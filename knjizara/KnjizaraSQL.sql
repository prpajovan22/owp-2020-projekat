use knjizara;
drop table if exists elementiKorpe;
drop table if exists knjigaZanr;
drop table if exists korpa;
drop table if exists zanrovi;
drop table if exists komentar;
drop table if exists izvestaj;
drop table if exists knjiga;
drop table if exists kartica;
drop table if exists korisnik;
drop table if exists SpecialDate;

create table knjiga(
ISBN varChar(13) unique not null,
name varChar(35) not null,
izdavackaKuca varchar(35) not null,
autor varchar(35) not null,
releaseYear date not null,
description varChar(350) not null,
picture varchar(255) not null,
price double,
pageCount int not null,
hard boolean not null,
latinica boolean not null,
language varchar(25) not null,
avgGrade double,
brojKnjiga int
);

create table zanrovi(
id SmallInt primary Key auto_increment,
name varChar(35),
description varChar(350) not null
);

create table knjigaZanr(
ISBN varchar(13) ,
zanrId SmallInt,
foreign key (ISBN) references knjiga(ISBN),
foreign key (zanrId) references zanrovi(id)
);

create table korisnik(
id SmallInt primary Key auto_increment,
username varchar(35),
password varchar(255),
email varchar(255),
firstname varchar(255),
lastname varchar(255),
birthDate date,
adress varchar(255),
phonenumber int,
registrationDate date,
admin boolean not null,
izblokiran boolean not null
);

create table korpa(
id smallint primary key auto_increment, 
korisnikId smallint,
foreign key (korisnikId) references korisnik(id),
status boolean not null
);

create table elementiKorpe(
id smallint primary key auto_increment,
brojKupovina int,
korpaId smallint,
knjigaISBN varChar(13),
foreign key (knjigaISBN) references knjiga(ISBN),
foreign key (korpaId) references korpa(id)
);

create table komentar(
id smallint primary key auto_increment,
text varchar(225) not null,
grade int not null,
Date date not null,
korisnikId smallint,
knjigaISBN varchar(13),
foreign key (korisnikId) references korisnik(id),
foreign key (knjigaISBN) references knjiga(ISBN),
status ENUM('waiting','approved','not_approved')
);

create table izvestaj(
id smallint primary key auto_increment,
prodataKnjiga varchar(13) ,
foreign key (prodataKnjiga) references knjiga(ISBN),
datumKupovine date not null,
brojProdatijh int not null,
cenaProdatih double not null
);

create table kartica(
id smallint primary key auto_increment,
discount double,
pointCount double,
korisnikId smallint,
foreign key (korisnikId) references korisnik(id),
statusKartice boolean not null 	
);

create table SpecialDate(
id smallint primary key auto_increment,
specialDate date,
popust int
)




