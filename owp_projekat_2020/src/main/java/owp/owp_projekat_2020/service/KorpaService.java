package owp.owp_projekat_2020.service;

import java.util.ArrayList;

import owp.owp_projekat_2020.models.ElementiKorpe;
import owp.owp_projekat_2020.models.Korpa;

public interface KorpaService {

	Korpa find(String korisnikId);

	Korpa save(Korpa korpa);

	ElementiKorpe saveElementKorpe(ElementiKorpe elementiKorpe);

	Korpa update(Korpa korpa);

	ArrayList<ElementiKorpe> findAllElemetns(String id);
	
	void deleteElement(String id);

}
