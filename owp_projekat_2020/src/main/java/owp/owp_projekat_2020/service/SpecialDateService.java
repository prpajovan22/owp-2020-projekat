package owp.owp_projekat_2020.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import owp.owp_projekat_2020.models.SpecialDate;


public interface SpecialDateService {
		
	SpecialDate save(SpecialDate sDate);
	
	SpecialDate findDate(Date specialDate);
	
	List<SpecialDate> findAll();
	
	List<SpecialDate> find(Date specialDate, int popust);


}
