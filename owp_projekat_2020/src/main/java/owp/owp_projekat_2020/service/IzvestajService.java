package owp.owp_projekat_2020.service;

import java.util.Date;
import java.util.List;

import owp.owp_projekat_2020.models.Izvestaj;
import owp.owp_projekat_2020.models.Knjiga;

public interface IzvestajService {

	Izvestaj save (Izvestaj izvestaj);
	
	Izvestaj update (Izvestaj izvestaj);
	
	Izvestaj findOne(Knjiga ISBN);
	
	List<Izvestaj> getAll();

	List<Izvestaj> findAll();

	List<Izvestaj> findAllByDateRange(String firstDate,String secondDate);
	
	List<Izvestaj> findAllByDateRangeAndSort(String firstDate,String secondDate,String sort,String order);

}
