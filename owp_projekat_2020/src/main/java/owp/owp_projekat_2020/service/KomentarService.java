package owp.owp_projekat_2020.service;

import java.util.List;

import owp.owp_projekat_2020.models.Komentar;

public interface KomentarService {

	Komentar save(Komentar komentar);

	Komentar update(Komentar komentar);

	List<Komentar> getAll(String ISBN);

	Komentar findOne(Integer id);
	
}
