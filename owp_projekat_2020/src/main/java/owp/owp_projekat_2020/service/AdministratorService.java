package owp.owp_projekat_2020.service;

import java.util.List;

import owp.owp_projekat_2020.models.Korisnik;

public interface AdministratorService {
	
	List <Korisnik> getAll();
	
	Korisnik getByUsername(String username);
	
	Korisnik update (Korisnik korisnik);

}
