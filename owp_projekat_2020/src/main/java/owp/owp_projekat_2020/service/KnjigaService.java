package owp.owp_projekat_2020.service;

import java.util.List;

import owp.owp_projekat_2020.models.Knjiga;

public interface KnjigaService {

	List<Knjiga> sortBy(String sortProperty, String sortOrder);

	List<Knjiga> getAll();

	Knjiga findOne(String ISBN);

	List<Knjiga> findAllSorted(String sortColumn, String sortingPosition);

	Knjiga getByISBN(String ISBN);

	Knjiga save(Knjiga knjiga);

	Knjiga update(Knjiga knjiga);
	
	Knjiga orderMoreBooks(Knjiga knjgia);

	Knjiga search(String ISBN);

	void delete(String ISBN);

	List<Knjiga> find(String ISBN, String name, String izdavackaKuca, String autor, Double price, Integer pageCount,
			String language, String sortColumn, String sortingPosition);

	List<Knjiga> findByZanrId(int id);

}
