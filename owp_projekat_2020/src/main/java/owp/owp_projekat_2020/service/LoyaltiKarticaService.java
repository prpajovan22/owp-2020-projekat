package owp.owp_projekat_2020.service;

import java.util.List;

import owp.owp_projekat_2020.models.Korisnik;
import owp.owp_projekat_2020.models.LoyaltiKartica;

public interface LoyaltiKarticaService {

	LoyaltiKartica save(LoyaltiKartica kartica);

	LoyaltiKartica update(LoyaltiKartica karica);

	LoyaltiKartica findOneCard(Korisnik korisnik);

	List<LoyaltiKartica> getAll(Integer id);
	
	LoyaltiKartica getById(Integer id);

}
