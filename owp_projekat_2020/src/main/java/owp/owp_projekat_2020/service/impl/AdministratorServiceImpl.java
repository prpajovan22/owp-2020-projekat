package owp.owp_projekat_2020.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import owp.owp_projekat_2020.dao.AdministratorDAO;
import owp.owp_projekat_2020.models.Korisnik;
import owp.owp_projekat_2020.service.AdministratorService;

public class AdministratorServiceImpl implements AdministratorService {
	
	@Autowired
	private AdministratorDAO  adminDAO;
	

	@Override
	public List<Korisnik> getAll() {
		List<Korisnik> korisnik = new ArrayList<Korisnik>();
		korisnik = adminDAO.findAll();
		return korisnik;
	}

	@Override
	public Korisnik getByUsername(String username) {
		Korisnik korisnik = adminDAO.findOne(username);
		return korisnik;
	}

	@Override
	public Korisnik update(Korisnik korisnik) {
		adminDAO.update(korisnik);
		return korisnik;
	}

}
