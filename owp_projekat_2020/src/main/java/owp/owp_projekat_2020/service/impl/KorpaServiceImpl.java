package owp.owp_projekat_2020.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import owp.owp_projekat_2020.dao.ElementKorpeDAO;
import owp.owp_projekat_2020.dao.KorpaDAO;
import owp.owp_projekat_2020.models.ElementiKorpe;
import owp.owp_projekat_2020.models.Korpa;
import owp.owp_projekat_2020.service.KorpaService;

@Service
public class KorpaServiceImpl implements KorpaService {

	@Autowired
	private KorpaDAO korpaDao;
	
	@Autowired
	private ElementKorpeDAO elementiKorpeDao;

	@Override
	public Korpa find(String korisnikId) {
		return korpaDao.findOneKorisnik(korisnikId);
	}

	@Override
	public Korpa save(Korpa korpa) {
		String idKorpe = korpaDao.save(korpa);
		korpa.setId(idKorpe);
		return korpa;
	}

	@Override
	public ElementiKorpe saveElementKorpe(ElementiKorpe elementiKorpe) {
		elementiKorpeDao.save(elementiKorpe);
		return elementiKorpe;
	}

	@Override
	public Korpa update(Korpa korpa) {
		korpaDao.update(korpa);
		return korpa;
	}

	@Override
	public ArrayList<ElementiKorpe> findAllElemetns(String id) {
		return korpaDao.findAllElemetns(id);
	}

	@Override
	public void deleteElement(String id) {
		korpaDao.deleteElement(id);
	}
}
