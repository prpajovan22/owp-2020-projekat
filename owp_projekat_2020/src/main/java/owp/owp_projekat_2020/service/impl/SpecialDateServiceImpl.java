package owp.owp_projekat_2020.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import owp.owp_projekat_2020.dao.KorisnikDAO;
import owp.owp_projekat_2020.dao.SpecialDateDAO;
import owp.owp_projekat_2020.models.SpecialDate;
import owp.owp_projekat_2020.service.SpecialDateService;

@Service
public class SpecialDateServiceImpl implements SpecialDateService {

	@Autowired
	private SpecialDateDAO sDateDao;

	@Override
	public SpecialDate save(SpecialDate sDate) {
		sDateDao.save(sDate);
		return sDate;
	}

	@Override
	public SpecialDate findDate(Date specialDate) {
		return sDateDao.findDate(specialDate);
	}

	@Override
	public List<SpecialDate> findAll() {
		return sDateDao.findAll();
	}

	@Override
	public List<SpecialDate> find(Date specialDate, int popust) {
		// TODO Auto-generated method stub
		return null;
	}

}
