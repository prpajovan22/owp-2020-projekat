package owp.owp_projekat_2020.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import owp.owp_projekat_2020.dao.LoyaltiKarticaDAO;
import owp.owp_projekat_2020.models.Knjiga;
import owp.owp_projekat_2020.models.Korisnik;
import owp.owp_projekat_2020.models.LoyaltiKartica;
import owp.owp_projekat_2020.service.LoyaltiKarticaService;



@Service
public class LoyaltyKarticaServiceImpl implements LoyaltiKarticaService {

	@Autowired
	private LoyaltiKarticaDAO karticaDao;
	
	@Override
	public LoyaltiKartica save(LoyaltiKartica kartica) {
		return null;
	}

	@Override
	public LoyaltiKartica update(LoyaltiKartica karica) {
		return null;
	}

	@Override
	public LoyaltiKartica findOneCard(Korisnik korisnik) {
		LoyaltiKartica kartica = karticaDao.findOneCard(korisnik);
		return kartica;
	}

	@Override
	public List<LoyaltiKartica> getAll(Integer id) {
		return null;
	}

	@Override
	public LoyaltiKartica getById(Integer id) {
		return null;
	}
}