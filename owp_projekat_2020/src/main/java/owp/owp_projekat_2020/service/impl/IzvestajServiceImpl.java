package owp.owp_projekat_2020.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import owp.owp_projekat_2020.dao.IzvestajDAO;
import owp.owp_projekat_2020.models.Izvestaj;
import owp.owp_projekat_2020.models.Knjiga;
import owp.owp_projekat_2020.service.IzvestajService;

@Service
public class IzvestajServiceImpl implements IzvestajService{

	@Autowired
	private IzvestajDAO izvestajDao;
	
	@Override
	public Izvestaj save(Izvestaj izvestaj) {
		izvestajDao.save(izvestaj);
		return izvestaj;
	}

	@Override
	public Izvestaj update(Izvestaj izvestaj) {
		izvestajDao.update(izvestaj);
		return izvestaj;
	}

	@Override
	public Izvestaj findOne(Knjiga prodataKnjiga) {
		return izvestajDao.findOne(prodataKnjiga);
		}

	@Override
	public List<Izvestaj> getAll() {
		List<Izvestaj> izvestaj = new ArrayList<Izvestaj>();
		izvestaj = izvestajDao.findAll();
		return izvestaj;
	}

	@Override
	public List<Izvestaj> findAll() {
		return izvestajDao.findAll();
	}

	@Override
	public List<Izvestaj> findAllByDateRange(String firstDate, String secondDate) {
		return izvestajDao.findAllByDateRange(firstDate, secondDate);
	}

	@Override
	public List<Izvestaj> findAllByDateRangeAndSort(String firstDate, String secondDate, String sort, String order) {
		return izvestajDao.findAllByDateRangeAndSort(firstDate, secondDate, sort, order);
	}


}
