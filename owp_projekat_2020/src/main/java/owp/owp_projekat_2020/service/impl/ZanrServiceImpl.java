package owp.owp_projekat_2020.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.util.ListUtils;


import owp.owp_projekat_2020.dao.ZanrDAO;
import owp.owp_projekat_2020.models.Zanr;
import owp.owp_projekat_2020.service.ZanrService;

@Service
public class ZanrServiceImpl implements ZanrService {
	
	@Autowired
	private ZanrDAO zanrDAO;
	
	@Override
	public Zanr findOne(int id) {
		return zanrDAO.findOne(id);
	}

	@Override
	public List<Zanr> findAll() {
		// TODO Auto-generated method stub
		return zanrDAO.findAll();
	}

	@Override
	public List<Zanr> save(List<Zanr> zanrovi) {
		zanrDAO.save((Zanr) zanrovi);
		return zanrovi;
	}

	@Override
	public Zanr update(Zanr zanr) {
		zanrDAO.update(zanr);
		return zanr;
	}

	@Override
	public List<Zanr> find(List<Integer> zanroviIds) {
		List<Zanr> zanrovi = new ArrayList<Zanr>();
		if(!ListUtils.isEmpty(zanroviIds)) {
			for (int zanrId : zanroviIds) {
				Zanr zanr = zanrDAO.findOne(zanrId);
				if(zanr!= null)
					zanrovi.add(zanr);
			}
		}
		return zanrovi;
	}
}
