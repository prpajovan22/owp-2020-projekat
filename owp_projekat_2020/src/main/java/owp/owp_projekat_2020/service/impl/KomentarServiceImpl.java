package owp.owp_projekat_2020.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import owp.owp_projekat_2020.dao.KomentarDAO;
import owp.owp_projekat_2020.models.Knjiga;
import owp.owp_projekat_2020.models.Komentar;
import owp.owp_projekat_2020.service.KomentarService;

@Service
public class KomentarServiceImpl implements KomentarService{

	@Autowired
	private KomentarDAO komentarDAO;
	
	@Override
	public Komentar save(Komentar komentar) {
		komentarDAO.save(komentar);
		return komentar;
	}

	@Override
	public Komentar update(Komentar komentar) {
		komentarDAO.update(komentar);
		return komentar;
	}

	@Override
	public List<Komentar> getAll(String ISBN) {
		List<Komentar> komentar = new ArrayList<Komentar>();
		komentar = komentarDAO.findAll(ISBN);
		return komentar;
	}

	@Override
	public Komentar findOne(Integer id) {
		return komentarDAO.findOne(id);
	}

}
