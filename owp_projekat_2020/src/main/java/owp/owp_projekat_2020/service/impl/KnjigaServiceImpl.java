package owp.owp_projekat_2020.service.impl;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import owp.owp_projekat_2020.dao.KnjigaDAO;
import owp.owp_projekat_2020.models.Knjiga;
import owp.owp_projekat_2020.service.KnjigaService;


@Service
public class KnjigaServiceImpl implements KnjigaService {
	
	@Autowired
	private KnjigaDAO knjigaDao;

	@Override
	public List<Knjiga> getAll() {
		return knjigaDao.findAll();
	}

	@Override
	public Knjiga getByISBN(String ISBN) {
		Knjiga knjiga = knjigaDao.findOne(ISBN);
		return knjiga;
	}

	@Override
	public Knjiga save(Knjiga knjiga) {
		 knjigaDao.save(knjiga);
		return knjiga;
	}

	@Override
	public Knjiga update(Knjiga knjiga) {
		knjigaDao.update(knjiga);
		return knjiga;
	}

	@Override
	public Knjiga search(String ISBN) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(String ISBN) {
		knjigaDao.delete(ISBN);
		
	}


	@Override
	public List<Knjiga> findByZanrId(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Knjiga findOne(String ISBN) {
		return knjigaDao.findOne(ISBN);
	}

	@Override
	public List<Knjiga> findAllSorted(String sortColumn, String sortingPosition) {
		return knjigaDao.findAllSorted(sortColumn, sortingPosition);
	}

	@Override
	public List<Knjiga> find(String ISBN, String name, String izdavackaKuca, String autor, Double price,
			Integer pageCount, String language,String sortColumn,String sortPosition) {
		return knjigaDao.find(ISBN, name,autor,price,language, sortColumn, sortPosition);
	}

	@Override
	public List<Knjiga> sortBy(String sortProperty, String sortOrder) {
		return null;
	}

	@Override
	public Knjiga orderMoreBooks(Knjiga knjgia) {
		knjigaDao.orderMoreBooks(knjgia);
		return knjgia;
	}

	

}
