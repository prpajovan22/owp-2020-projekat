package owp.owp_projekat_2020.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import owp.owp_projekat_2020.dao.KorisnikDAO;
import owp.owp_projekat_2020.models.Korisnik;
import owp.owp_projekat_2020.service.KorisnikService;

@Service
public class KorisnikServiceImp implements KorisnikService{
	
	@Autowired
	private KorisnikDAO korisnikDAO;

	@Override
	public Korisnik findOne(String username) {
		return korisnikDAO.findOneUser(username);
	}

	@Override
	public Korisnik findOne(String username, String password) {
		return korisnikDAO.findOne(username, password);
	}

	@Override
	public List<Korisnik> findAll() {
		return korisnikDAO.findAll();
	}

	@Override
	public Korisnik save(Korisnik korisnik) {
		korisnikDAO.save(korisnik);
		return korisnik;
	}

	@Override
	public List<Korisnik> save(List<Korisnik> korisnici) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public List<Korisnik> update(List<Korisnik> korisnici) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void delete(List<String> username) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Korisnik> find(String username, String password, String email, String firstname, String lastname,
			Date birthDate, String adress, Integer phonenumber, Date registrationDate, Boolean admin,Boolean izblokiran) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Korisnik> findByKorisnickoIme(String username) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Korisnik> find(String korisnickoIme, String lozinka, Boolean administrator) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Korisnik update(Korisnik korisnik) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Korisnik delete(String username) {
		// TODO Auto-generated method stub
		return null;
	}

}
