package owp.owp_projekat_2020.service;

import java.util.List;

import owp.owp_projekat_2020.models.Zanr;

public interface ZanrService {

	Zanr findOne(int id); 
	
	List<Zanr> find(List<Integer>zanroviIds);
	
	List<Zanr> findAll();
	
	List<Zanr> save(List<Zanr> zanrovi);
	
	Zanr update(Zanr zanr);
	
}
