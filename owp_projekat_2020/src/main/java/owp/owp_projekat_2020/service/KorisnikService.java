package owp.owp_projekat_2020.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import owp.owp_projekat_2020.models.Korisnik;

public interface KorisnikService {
	Korisnik findOne(String username);

	Korisnik findOne(String username, String lozinka);

	List<Korisnik> findAll();

	Korisnik save(Korisnik korisnik);

	List<Korisnik> save(List<Korisnik> korisnici);

	Korisnik update(Korisnik korisnik);

	List<Korisnik> update(List<Korisnik> korisnici);

	Korisnik delete(String username);

	void delete(List<String> username);

	List<Korisnik> find(String username, String lozinka, Boolean administrator);

	List<Korisnik> findByKorisnickoIme(String username);

	List<Korisnik> find(String username, String password, String email, String firstname, String lastname,
			Date birthDate, String adress, Integer phonenumber, Date registrationDate, Boolean admin, Boolean izblokiran);

}
