package owp.owp_projekat_2020;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OwpProjekat2020Application {

	public static void main(String[] args) {
		SpringApplication.run(OwpProjekat2020Application.class, args);
	}
}
