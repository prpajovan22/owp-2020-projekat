package owp.owp_projekat_2020.models;

public class KupljenaKnjiga {

	private int id;
	private Knjiga book;
	private int numberOfCopies;
	private double cena;

	public KupljenaKnjiga(int id, Knjiga book, int numberOfCopies, double cena) {
		super();
		this.id = id;
		this.book = book;
		this.numberOfCopies = numberOfCopies;
		this.cena = cena;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public Knjiga getBook() {
		return book;
	}


	public void setBook(Knjiga book) {
		this.book = book;
	}


	public int getNumberOfCopies() {
		return numberOfCopies;
	}


	public void setNumberOfCopies(int numberOfCopies) {
		this.numberOfCopies = numberOfCopies;
	}


	public double getCena() {
		return cena;
	}


	public void setCena(double cena) {
		this.cena = cena;
	}
	
}
