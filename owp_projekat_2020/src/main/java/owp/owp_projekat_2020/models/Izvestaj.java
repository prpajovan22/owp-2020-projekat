package owp.owp_projekat_2020.models;

import java.util.Date;

public class Izvestaj {

	private int id;
	private Knjiga prodataKnjiga;
	private Date datumKupovine;
	private int brojProdatijh;
	private double cenaProdatih;

	public Izvestaj(int id, Knjiga prodataKnjiga, Date datumKupovine, int brojProdatih, double cenaProdatih) {
		super();
		this.id = id;
		this.prodataKnjiga = prodataKnjiga;
		this.datumKupovine = datumKupovine;
		this.brojProdatijh = brojProdatih;
		this.cenaProdatih = cenaProdatih;
	}

	public double getCenaProdatih() {
		return cenaProdatih;
	}

	public void setCenaProdatih(double cenaProdatih) {
		this.cenaProdatih = cenaProdatih;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getBrojProdatijh() {
		return brojProdatijh;
	}

	public void setBrojProdatijh(int brojProdatijh) {
		this.brojProdatijh = brojProdatijh;
	}

	public Knjiga getProdataKnjiga() {
		return prodataKnjiga;
	}

	public void setProdataKnjiga(Knjiga prodataKnjiga) {
		this.prodataKnjiga = prodataKnjiga;
	}

	public Date getDatumKupovine() {
		return datumKupovine;
	}

	public void setDatumKupovine(Date datumKupovine) {
		this.datumKupovine = datumKupovine;
	}

}
