package owp.owp_projekat_2020.models;

import java.util.Date;

public class Korisnik {

	private String id;
	private String username;
	private String password;
	private String email;
	private String firstname;
	private String lastname;
	private Date birthDate;
	private String adress;
	private int phonenumber;
	private Date registrationDate;
	private boolean admin;
	private boolean izblokiran;

	public Korisnik(String id, String username, String password, String email, String firstname, String lastname,
			Date birthDate, String adress, int phonenumber, Date registrationDate, boolean admin, boolean izblokiran) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.email = email;
		this.firstname = firstname;
		this.lastname = lastname;
		this.birthDate = birthDate;
		this.adress = adress;
		this.phonenumber = phonenumber;
		this.registrationDate = registrationDate;
		this.admin = admin;
		this.izblokiran = izblokiran;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getAdress() {
		return adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}

	public int getPhonenumber() {
		return phonenumber;
	}

	public void setPhonenumber(int phonenumber) {
		this.phonenumber = phonenumber;
	}

	public Date getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}

	public boolean isAdmin() {
		return admin;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	public boolean isIzblokiran() {
		return izblokiran;
	}

	public void setIzblokiran(boolean izblokiran) {
		this.izblokiran = izblokiran;
	}

}
