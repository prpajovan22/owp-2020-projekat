package owp.owp_projekat_2020.models;

public class ElementiKorpe {

	private String id;
	private int brojKupovina;
	private Knjiga knjiga;
	private Korpa korpa;

	public ElementiKorpe(String id, int brojKupovina, Knjiga knjiga, Korpa korpa) {
		super();
		this.id = id;
		this.brojKupovina = brojKupovina;
		this.knjiga = knjiga;
		this.korpa = korpa;
	}

	public Knjiga getKnjiga() {
		return knjiga;
	}

	public void setKnjiga(Knjiga knjiga) {
		this.knjiga = knjiga;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getId() {
		return id;
	}

	public int getBrojKupovina() {
		return brojKupovina;
	}

	public void setBrojKupovina(int brojKupovina) {
		this.brojKupovina = brojKupovina;
	}

	public Korpa getKorpa() {
		return korpa;
	}

	public void setKorpa(Korpa korpa) {
		this.korpa = korpa;
	}

}
