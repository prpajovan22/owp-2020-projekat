package owp.owp_projekat_2020.models;

public class LoyaltiKartica {

	private Integer id;
	private double discount;
	private double pointCount;
	private Korisnik korisnik;
	private boolean statusKartice;

	public LoyaltiKartica(Integer id, double discount, double pointCount, Korisnik korisnik, boolean statusKartice) {
		super();
		this.id = id;
		this.discount = discount;
		this.pointCount = pointCount;
		this.korisnik = korisnik;
		this.statusKartice = statusKartice;
	}

	public Korisnik getKorisnik() {
		return korisnik;
	}

	public void setKorisnik(Korisnik korisnik) {
		this.korisnik = korisnik;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}

	public double getPointCount() {
		return pointCount;
	}

	public void setPointCount(double pointCount) {
		this.pointCount = pointCount;
	}

	public boolean isStatusKartice() {
		return statusKartice;
	}

	public void setStatusKartice(boolean statusKartice) {
		this.statusKartice = statusKartice;
	}

}
