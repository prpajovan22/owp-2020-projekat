package owp.owp_projekat_2020.models;

public class Zanr {
	
	private int id;
	private String name;
	private String description;

	public Zanr(int id, String name, String description) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}

	

	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}
}
