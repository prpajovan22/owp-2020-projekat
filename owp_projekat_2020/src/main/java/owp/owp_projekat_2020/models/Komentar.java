package owp.owp_projekat_2020.models;

import java.util.Date;

public class Komentar {

	private Integer id;
	private String text;
	private int grade;
	private Date date;
	private Korisnik autor;
	private Knjiga knjiga;
	private StatusTip status = StatusTip.waiting;
	
	public Komentar(Integer id,String text, int grade, Date createdOn, Korisnik autor, Knjiga knjiga, StatusTip status) {
		super();
		this.id = id;
		this.text = text;
		this.grade = grade;
		this.date = createdOn;
		this.autor = autor;
		this.knjiga = knjiga;
		this.status = status;
	}
	

	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public int getGrade() {
		return grade;
	}

	public void setGrade(int grade) {
		this.grade = grade;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Korisnik getAutor() {
		return autor;
	}

	public void setAutor(Korisnik autor) {
		this.autor = autor;
	}

	public Knjiga getKnjiga() {
		return knjiga;
	}

	public void setKnjiga(Knjiga knjiga) {
		this.knjiga = knjiga;
	}

	public StatusTip getStatus() {
		return status;
	}

	public void setStatus(StatusTip status) {
		this.status = status;
	}
	
}
