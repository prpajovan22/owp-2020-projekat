package owp.owp_projekat_2020.models;

public class Korpa {

	private String id;
	private Korisnik korisnik;
	private boolean status;
	
	public Korpa(String id, Korisnik korisnik, boolean status) {
		super();
		this.id = id;
		this.korisnik = korisnik;
		this.status = status;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Korisnik getKorisnik() {
		return korisnik;
	}
	public void setKorisnik(Korisnik korisnik) {
		this.korisnik = korisnik;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}

}
