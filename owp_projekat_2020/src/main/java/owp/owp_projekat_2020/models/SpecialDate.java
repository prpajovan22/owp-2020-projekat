package owp.owp_projekat_2020.models;

import java.util.Date;

public class SpecialDate {

	private String id;
	private Date specialDate;
	private int popust;

	public SpecialDate(String id, Date specialDate,int popust) {
		super();
		this.id = id;
		this.specialDate = specialDate;
		this.popust = popust;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getSpecialDate() {
		return specialDate;
	}

	public void setSpecialDate(Date specialDate) {
		this.specialDate = specialDate;
	}

	public int getPopust() {
		return popust;
	}

	public void setPopust(int popust) {
		this.popust = popust;
	}

}
