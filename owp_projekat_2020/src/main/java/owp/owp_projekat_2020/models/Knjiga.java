package owp.owp_projekat_2020.models;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class Knjiga {

	private String ISBN;
	private String name;
	private String izdavackaKuca;
	private String autor;
	private Date releaseYear;
	private String description;
	private String picture;
	private double price;
	private int pageCount;
	private boolean hard;
	private boolean latinica;
	private String language;
	private double avgGrade;
	private int brojKnjiga;

	private List<Zanr> zanr = new ArrayList<>();

	public Knjiga(String iSBN, String name, String izdavackaKuca, String autor, Date releaseYear, String description,
			String picture, double price, int pageCount, boolean hard, boolean latinica, String language,
			double avgGrade, int brojKnjiga) {
		super();
		ISBN = iSBN;
		this.name = name;
		this.izdavackaKuca = izdavackaKuca;
		this.autor = autor;
		this.releaseYear = releaseYear;
		this.description = description;
		this.picture = picture;
		this.price = price;
		this.pageCount = pageCount;
		this.hard = hard;
		this.latinica = latinica;
		this.language = language;
		this.avgGrade = avgGrade;
		this.brojKnjiga = brojKnjiga;
	}

	public List<Zanr> getZanr() {
		return zanr;
	}

	public void setZanr(List<Zanr> zanr) {
		this.zanr.clear();
		this.zanr.addAll(zanr);
	}

	public String getISBN() {
		return ISBN;
	}

	public void setISBN(String iSBN) {
		ISBN = iSBN;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIzdavackaKuca() {
		return izdavackaKuca;
	}

	public void setIzdavackaKuca(String izdavackaKuca) {
		this.izdavackaKuca = izdavackaKuca;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public Date getReleaseYear() {
		return releaseYear;
	}

	public void setReleaseYear(Date releaseYear) {
		this.releaseYear = releaseYear;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getPageCount() {
		return pageCount;
	}

	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}

	public boolean isHard() {
		return hard;
	}

	public void setHard(boolean hard) {
		this.hard = hard;
	}

	public boolean isLatinica() {
		return latinica;
	}

	public void setLatinica(boolean latinica) {
		this.latinica = latinica;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public double getAvgGrade() {
		return avgGrade;
	}

	public void setAvgGrade(double avgGrade) {
		this.avgGrade = avgGrade;
	}

	public int getBrojKnjiga() {
		return brojKnjiga;
	}

	public void setBrojKnjiga(int brojKnjiga) {
		this.brojKnjiga = brojKnjiga;
	}

}
