package owp.owp_projekat_2020.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import owp.owp_projekat_2020.models.Knjiga;
import owp.owp_projekat_2020.models.Komentar;
import owp.owp_projekat_2020.models.Korisnik;
import owp.owp_projekat_2020.models.StatusTip;
import owp.owp_projekat_2020.service.KnjigaService;
import owp.owp_projekat_2020.service.KomentarService;

@Controller
@RequestMapping(value = "/Komentar")
public class KomentarController {

	public static final String KORISNIK_KEY = "prijavljeniKorisnik";

	@Autowired
	private ServletContext servletContext;

	@Autowired
	private KomentarService komentarService;

	@Autowired
	private KnjigaService knjigaService;

	private String baseURL;

	@PostConstruct
	public void init() {
		baseURL = servletContext.getContextPath() + "/";
	}

	@GetMapping
	public ModelAndView index( HttpSession session,
			HttpServletResponse response) throws IOException {
		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		if (prijavljeniKorisnik == null || !prijavljeniKorisnik.isAdmin()) {
			response.sendRedirect(baseURL);
			return null;
		}
		List<Komentar> komentar = komentarService.getAll(null);

		ModelAndView rezultat = new ModelAndView("komentar");
		rezultat.addObject("komentari", komentar);
		return rezultat;

	}

	@GetMapping(value = "/Create")
	public ModelAndView create(HttpSession session, HttpServletResponse response) {

		ModelAndView rezultat = new ModelAndView("dodavanjeKomentara");
		return rezultat;
	}

	@PostMapping(value = "/Create")
	public void create(@RequestParam String text, @RequestParam Integer grade, @RequestParam String ISBN,
			HttpSession session, HttpServletResponse response) {

		Korisnik korisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		StatusTip status = StatusTip.waiting;
		Knjiga knjiga = knjigaService.findOne(ISBN);
		java.util.Date createdOn = new java.util.Date();
		Komentar komentar = new Komentar(null, text, grade, createdOn, korisnik, knjiga, status);
		komentarService.save(komentar);
		
		List<Komentar> komentari = komentarService.getAll(knjiga.getISBN());
		
		double sumaOcena = 0;
		for(Komentar koment: komentari) {
			sumaOcena+= koment.getGrade();
		}
		
		double prosecnaOcena = sumaOcena / komentari.size();
		
		knjiga.setAvgGrade(prosecnaOcena);
		
		knjigaService.update(knjiga);
		try {
			response.sendRedirect(baseURL + "Knjige");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@GetMapping(value = "/Edit")
	public ModelAndView edit(HttpSession session, HttpServletResponse response) {

		ModelAndView rezultat = new ModelAndView("komentar");

		return rezultat;
	}

	@PostMapping(value = "/Edit")
	public void edit(@RequestParam String status, @RequestParam Integer id, HttpSession session,
			HttpServletResponse response) throws IOException {

		Komentar komentar = komentarService.findOne(id);
		if (komentar == null) {
			response.sendRedirect(baseURL + "index");
			return;
		}
		StatusTip newStatus = StatusTip.valueOf(status);

		komentar.setStatus(newStatus);

		komentarService.update(komentar);

		response.sendRedirect(baseURL + "index");

	}

	@GetMapping(value = "/Details")
	public ModelAndView details(@RequestParam String ISBN, HttpSession session, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		ModelAndView rezultat = new ModelAndView("komentar");
		Korisnik korisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		List<Komentar> komentariZaPrikaz = new ArrayList<Komentar>();
		List<Komentar> komentari = komentarService.getAll(ISBN);
		for (Komentar komentar : komentari) {
			if (komentar.getStatus().equals(StatusTip.approved)) {
				komentariZaPrikaz.add(komentar);
			}
		}
		rezultat.addObject("komentari", komentariZaPrikaz);
		return rezultat;
	}

}
