package owp.owp_projekat_2020.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import owp.owp_projekat_2020.models.ElementiKorpe;
import owp.owp_projekat_2020.models.Izvestaj;
import owp.owp_projekat_2020.models.Knjiga;
import owp.owp_projekat_2020.models.Komentar;
import owp.owp_projekat_2020.models.Korisnik;
import owp.owp_projekat_2020.models.Korpa;
import owp.owp_projekat_2020.models.LoyaltiKartica;
import owp.owp_projekat_2020.models.StatusTip;
import owp.owp_projekat_2020.service.KorisnikService;
import owp.owp_projekat_2020.service.LoyaltiKarticaService;

@Controller
@RequestMapping(value = "LoyaltiKartica")
public class LoyaltiKarticaController {

	private ServletContext servletContext;
	private final LoyaltiKarticaService karticaService;
	private final KorisnikService korisnikService;

	private String baseURL;

	@Autowired
	public LoyaltiKarticaController(ServletContext servletContext, LoyaltiKarticaService karticaService,
			KorisnikService korisnikService) {
		this.servletContext = servletContext;
		this.karticaService = karticaService;
		this.korisnikService = korisnikService;
	}

	@PostConstruct
	public void init() {
		baseURL = servletContext.getContextPath() + "/";
	}

	@GetMapping
	public ModelAndView index(@RequestParam int id, HttpSession session, HttpServletResponse response)
			throws IOException {
		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		if (prijavljeniKorisnik == null || !prijavljeniKorisnik.isAdmin()) {
			response.sendRedirect(baseURL);
			return null;
		}
		List<LoyaltiKartica> kartica = karticaService.getAll(id);

		ModelAndView rezultat = new ModelAndView("kartica");
		rezultat.addObject("kartice", kartica);
		return rezultat;

	}

	@PostMapping(value = "/NewCard")
	public void create(HttpSession session, HttpServletResponse response) {

		Korisnik korisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		LoyaltiKartica kartica = new LoyaltiKartica(null, 4, 20, korisnik, false);

		try {
			response.sendRedirect(baseURL + "Knjige");
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@GetMapping(value = "/Izmena")
	public ModelAndView izmena(@RequestParam int id, HttpSession session, HttpServletResponse response) {

		LoyaltiKartica kartica = karticaService.getById(id);
		ModelAndView rezultat = new ModelAndView("izmenaKartice");
		rezultat.addObject("kartica", kartica);

		return rezultat;
	}

	@PostMapping(value = "/Izmena")
	public void izmena(@RequestParam int id, @RequestParam Boolean statusKartice, HttpSession session,
			HttpServletResponse response) throws IOException {

		LoyaltiKartica kartica = karticaService.getById(id);
		if (kartica == null) {
			response.sendRedirect(baseURL + "LoyaltiKartica");
			return;
		}
		if (statusKartice == null) {
			response.sendRedirect(baseURL + "LoyaltiKartica/Details?id=" + id);
			return;
		}

		kartica.setStatusKartice(statusKartice);

		karticaService.update(kartica);

		response.sendRedirect(baseURL + "LoyaltiKartica");

	}
}
