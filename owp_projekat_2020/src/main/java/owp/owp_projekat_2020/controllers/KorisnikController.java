package owp.owp_projekat_2020.controllers;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import owp.owp_projekat_2020.models.Korisnik;
import owp.owp_projekat_2020.models.Korpa;
import owp.owp_projekat_2020.models.LoyaltiKartica;
import owp.owp_projekat_2020.service.KorisnikService;
import owp.owp_projekat_2020.service.KorpaService;
import owp.owp_projekat_2020.service.LoyaltiKarticaService;

@Controller
@RequestMapping(value = "/Korisnici")
public class KorisnikController {

	public static final String KORISNIK_KEY = "prijavljeniKorisnik";

	@Autowired
	private KorisnikService korisnikService;

	@Autowired
	private ServletContext servletContext;

	@Autowired
	private KorpaService korpaService;

	@Autowired
	private LoyaltiKarticaService karticaService;

	private String baseURL;

	@PostConstruct
	public void init() {
		baseURL = servletContext.getContextPath() + "/";
	}

	@GetMapping
	public ModelAndView index(@RequestParam(required = false) String username,
			@RequestParam(required = false) String password, @RequestParam(required = false) String email,
			@RequestParam(required = false) String firstname, @RequestParam(required = false) String lastname,
			@RequestParam(required = false) Date birthDate, @RequestParam(required = false) String adress,
			@RequestParam(required = false) Integer phonenumber, @RequestParam(required = false) Date registrationDate,
			@RequestParam(required = false) Boolean admin, @RequestParam(required = false) Boolean izblokiran,
			HttpSession session, HttpServletResponse response) throws IOException {
		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		if (prijavljeniKorisnik == null || !prijavljeniKorisnik.isAdmin()) {
			response.sendRedirect(baseURL);
			return null;
		}
		if (username != null && username.trim().equals(""))
			username = null;

		if (password != null && password.trim().equals(""))
			password = null;

		if (email != null && email.trim().equals(""))
			email = null;

		if (firstname != null && firstname.trim().equals(""))
			firstname = null;

		if (lastname != null && lastname.trim().equals(""))
			lastname = null;

		if (birthDate != null && birthDate.equals(00 / 00 / 0000))
			birthDate = null;

		if (adress != null && adress.trim().equals(""))
			adress = null;

		if (phonenumber != null && phonenumber.equals(0))
			phonenumber = null;

		if (registrationDate != null && registrationDate.equals(00 / 00 / 0000))
			registrationDate = null;

		List<Korisnik> korisnici = korisnikService.find(username, password, firstname, lastname, adress, birthDate,
				adress, phonenumber, registrationDate, admin, izblokiran);

		ModelAndView rezultat = new ModelAndView("korisnici");
		rezultat.addObject("korisnici", korisnici);

		return rezultat;

	}

	@GetMapping(value = "/Details")
	public ModelAndView details(@RequestParam String username, HttpSession session, HttpServletResponse response)
			throws IOException {
		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		if (prijavljeniKorisnik == null
				|| (!prijavljeniKorisnik.isAdmin() && !prijavljeniKorisnik.getUsername().equals(username))) {
			response.sendRedirect(baseURL + "korisnik");
			return null;
		}

		Korisnik korisnik = korisnikService.findOne(username);
		if (korisnik == null) {
			response.sendRedirect(baseURL + "Korisnici");
			return null;
		}

		ModelAndView rezultat = new ModelAndView("korisnik");
		rezultat.addObject("korisnik", korisnik);

		return rezultat;
	}

	@PostMapping(value = "/Login")
	public ModelAndView postLogin(@RequestParam String username, @RequestParam String password, HttpSession session,
			HttpServletResponse response) throws IOException {
		try {
			Korisnik korisnik = korisnikService.findOne(username, password);
			if (korisnik == null) {
				throw new Exception("Neispravno korisničko ime ili lozinka!");
			} else if (korisnik.isIzblokiran() == true) {
				throw new Exception("Ne mozete se logovati jer ste blokirani!");
			}

			session.setAttribute(KorisnikController.KORISNIK_KEY, korisnik);

			response.sendRedirect(baseURL);
			return null;
		} catch (Exception ex) {
			String poruka = ex.getMessage();
			if (poruka == "") {
				poruka = "Neuspešna prijava!";
			}

			ModelAndView rezultat = new ModelAndView("prijava");
			rezultat.addObject("poruka", poruka);

			return rezultat;
		}
	}

	@GetMapping(value = "/Login")
	public ModelAndView loginPage(HttpSession session, HttpServletResponse response) {

		ModelAndView rezultat = new ModelAndView("prijava");

		return rezultat;
	}

	@GetMapping(value = "/Register")
	public ModelAndView registerPage(HttpSession session, HttpServletResponse response) {

		ModelAndView rezultat = new ModelAndView("registracija");

		return rezultat;
	}

	@PostMapping(value = "/Register")
	public ModelAndView register(@RequestParam String username, @RequestParam String password,
			@RequestParam String email, @RequestParam String firstname, @RequestParam String lastname,
			@RequestParam Date birthDate, @RequestParam String adress, @RequestParam int phonenumber,
			@RequestParam String ponovljenaLozinka, HttpSession session,
			HttpServletResponse response) throws IOException {
		try {
			Korisnik postojeciKorisnik = korisnikService.findOne(username);
			if (postojeciKorisnik != null) {
				throw new Exception("Korisničko ime već postoji!");
			}
			if (username.equals("") || password.equals("")) {
				throw new Exception("Korisničko ime i lozinka ne smeju biti prazni!");
			}
			if (!password.equals(ponovljenaLozinka)) {
				throw new Exception("Lozinke se ne podudaraju!");
			}
			if (email.equals("")) {
				throw new Exception("E-mail ne sme biti prazan!");
			}
			if (firstname.equals("")) {
				throw new Exception("Ime ne sme biti prazno!");
			}
			if (lastname.equals("")) {
				throw new Exception("Prezime ne sme biti prazno!");
			}
			if (adress.equals("")) {
				throw new Exception("Adresa ne sme biti prazna!");
			}
			if (birthDate.equals("00/00/0000")) {
				throw new Exception("Datum ne sme biti prazan!");
			}
			if (phonenumber == 0) {
				throw new Exception("Adresa ne sme biti prazna!");
			}

			Korisnik korisnik = new Korisnik("", username, password, email, firstname, lastname, birthDate, adress,
					phonenumber, new Date(), false, false);
			korisnikService.save(korisnik);

			response.sendRedirect(baseURL + "/Login");
			return null;
		} catch (Exception ex) {
			String poruka = ex.getMessage();
			if (poruka == "") {
				poruka = "Neuspešna registracija!";
			}

			ModelAndView rezultat = new ModelAndView("registracija");
			rezultat.addObject("poruka", poruka);

			return rezultat;
		}
	}

	@GetMapping(value = "/Profile")
	public ModelAndView details(HttpSession session, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Korisnik korisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		LoyaltiKartica kartica = karticaService.findOneCard(korisnik);
		ModelAndView rezultat = new ModelAndView("korisnik");
		rezultat.addObject("korisnik", korisnik);

		if (kartica != null) {
			rezultat.addObject("kartica", true);
		} else {
			rezultat.addObject("kartica", false);
		}
		return rezultat;
	}

	@GetMapping(value = "/EditKorisnik")
	public ModelAndView edit(HttpSession session, HttpServletResponse response) {

		Korisnik korisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		ModelAndView rezultat = new ModelAndView("korisnik");
		rezultat.addObject("korisnik", korisnik);

		return rezultat;
	}

	@PostMapping(value = "/EditKorisnik")
	public void edit(@RequestParam Boolean admin, @RequestParam Boolean izblokiran, HttpSession session,
			HttpServletResponse response) throws IOException {

		Korisnik korisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		if (korisnik == null) {
			response.sendRedirect(baseURL + "Korisnik");
			return;
		}
		if (admin == null || izblokiran == null) {
			response.sendRedirect(baseURL + "Korisnik/Profile?username=" + korisnik);
			return;
		}

		korisnik.setAdmin(admin);
		korisnik.setIzblokiran(izblokiran);

		korisnikService.update(korisnik);

		response.sendRedirect(baseURL + "Korisnik");

	}

	@GetMapping(value = "/Logout")
	public void logout(HttpSession session, HttpServletResponse response) throws IOException {

		Korisnik korisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		if (korisnik != null) {
			Korpa korpa = korpaService.find(korisnik.getId());
			if (korpa != null) {
				korpaService.update(korpa);
			}

		}

		session.invalidate();

		response.sendRedirect(baseURL);
	}
}
