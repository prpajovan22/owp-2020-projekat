package owp.owp_projekat_2020.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import owp.owp_projekat_2020.models.ElementiKorpe;
import owp.owp_projekat_2020.models.Izvestaj;
import owp.owp_projekat_2020.models.Knjiga;
import owp.owp_projekat_2020.models.Komentar;
import owp.owp_projekat_2020.models.Korisnik;
import owp.owp_projekat_2020.models.Korpa;
import owp.owp_projekat_2020.models.StatusTip;
import owp.owp_projekat_2020.service.IzvestajService;
import owp.owp_projekat_2020.service.KnjigaService;
import owp.owp_projekat_2020.service.KorpaService;

@Controller
@RequestMapping(value = "Izvestaj")
public class IzvestajController {

	public static final String KORISNIK_KEY = "prijavljeniKorisnik";

	@Autowired
	private ServletContext servletContext;

	@Autowired
	private KnjigaService knjigaService;

	@Autowired
	private IzvestajService izvestajService;

	@Autowired
	private KorpaService korpaService;

	private String baseURL;

	@PostConstruct
	public void init() {
		baseURL = servletContext.getContextPath() + "/";
	}

	@GetMapping
	public ModelAndView index(@RequestParam(required = false) String firstDate, @RequestParam(required = false) String secondDate,
			@RequestParam(required = false) String sort,@RequestParam(required = false) String order
			,HttpSession session, HttpServletResponse response) throws IOException {
		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		if (prijavljeniKorisnik == null || !prijavljeniKorisnik.isAdmin()) {
			response.sendRedirect(baseURL);
			return null;
		}
		if (secondDate != null && secondDate.trim().equals(""))
			secondDate = null;
		
		if (firstDate != null && firstDate.trim().equals(""))
			firstDate = null;
		
		if(firstDate == null || secondDate == null) {
			ModelAndView rezultat = new ModelAndView("izvestaj");
			ArrayList<Izvestaj> izv = (ArrayList<Izvestaj>) izvestajService.findAll();
			rezultat.addObject("izvestaji", izv);
			return rezultat;
		}
		
		List<Izvestaj> izvestaj = izvestajService.getAll();
		if (sort != null && sort.trim().equals(""))
			sort = null;

		if (order != null && order.trim().equals(""))
			order = null;
		
		if (sort ==null && order ==null){
			izvestaj = izvestajService.findAllByDateRange(firstDate,secondDate);
		}else if(sort !=null && order ==null)
		{
			izvestaj = izvestajService.findAllByDateRangeAndSort(firstDate,secondDate,sort,"asc");
			
		}else
		{
			izvestaj = izvestajService.findAllByDateRangeAndSort(firstDate,secondDate,sort,order);
		}

		ModelAndView rezultat = new ModelAndView("izvestaj");
		rezultat.addObject("izvestaji", izvestaj);
		return rezultat;

	}

	@GetMapping(value = "/Details")
	public ModelAndView details(@RequestParam String id, HttpSession session, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		ModelAndView rezultat = new ModelAndView("komentar");
		Korisnik korisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		List<Izvestaj> izvestaj = new ArrayList<Izvestaj>();

		rezultat.addObject("izvestaji", izvestaj);
		return rezultat;
	}

	
}
