package owp.owp_projekat_2020.controllers;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import owp.owp_projekat_2020.models.Knjiga;
import owp.owp_projekat_2020.models.Korisnik;
import owp.owp_projekat_2020.models.SpecialDate;
import owp.owp_projekat_2020.models.Zanr;
import owp.owp_projekat_2020.service.KorisnikService;
import owp.owp_projekat_2020.service.SpecialDateService;

@Controller
@RequestMapping(value = "/SpecialDate")
public class SpecialDateController {

	public static final String KORISNIK_KEY = "prijavljeniKorisnik";

	@Autowired
	private KorisnikService korisnikService;

	@Autowired
	private ServletContext servletContext;

	@Autowired
	private SpecialDateService sDateService;


	private String baseURL;
	
	@PostConstruct
	public void init() {
		baseURL = servletContext.getContextPath() + "/";
	}

	@GetMapping
	public ModelAndView index(@RequestParam(required = false) Date specialDate, @RequestParam(required = false) int popust,
			HttpSession session, HttpServletResponse response) throws IOException {
		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		if (prijavljeniKorisnik == null || !prijavljeniKorisnik.isAdmin()) {
			response.sendRedirect(baseURL);
			return null;
		}
		if (specialDate != null && specialDate.equals(00 / 00 / 0000))
			specialDate = null;

		if (popust > 0 && popust < 0)
			popust = 0;

		List<SpecialDate> sDate = sDateService.find(specialDate,popust);

		ModelAndView rezultat = new ModelAndView("specialDate");
		rezultat.addObject("specialDate", sDate);

		return rezultat;

	}
	
	@GetMapping(value = "/Details")
	public ModelAndView details(@RequestParam Date sDate, HttpSession session, HttpServletResponse response)
			throws IOException {

		SpecialDate specialDate = sDateService.findDate(sDate);
		if (sDate == null) {
			response.sendRedirect(baseURL + "SpecialDate");
			return null;
		}

		ModelAndView rezultat = new ModelAndView("specialDate");
		rezultat.addObject("specialDate", sDate);

		return rezultat;
	}
	
	@GetMapping(value = "/NewDate")
	public ModelAndView newDate(HttpSession session, HttpServletResponse response) {

		List<SpecialDate> sDate = sDateService.findAll();

		ModelAndView rezultat = new ModelAndView("dodavanjeSpecijanogDatuma");
		rezultat.addObject("specijaniDatum", sDate);

		return rezultat;
	}

	@PostMapping(value = "/NewDate")
	public void newDate(@RequestParam Date specialDate, @RequestParam int popust, HttpSession session,
			HttpServletResponse response) {

		SpecialDate sDate = new SpecialDate("",specialDate, popust);
		sDateService.save(sDate);

		try {
			response.sendRedirect(baseURL + "index");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
