package owp.owp_projekat_2020.controllers;

import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import owp.owp_projekat_2020.models.Knjiga;
import owp.owp_projekat_2020.models.Komentar;
import owp.owp_projekat_2020.models.Korisnik;
import owp.owp_projekat_2020.models.StatusTip;
import owp.owp_projekat_2020.models.Zanr;
import owp.owp_projekat_2020.service.KnjigaService;
import owp.owp_projekat_2020.service.KomentarService;
import owp.owp_projekat_2020.service.ZanrService;

@Controller
@RequestMapping(value = "/Knjige")
public class KnjigaController {

	@Autowired
	private KnjigaService knjigaService;

	@Autowired
	private ZanrService zanrService;

	@Autowired
	private KomentarService komentarService;

	@Autowired
	private ServletContext servletContext;
	private String baseURL;

	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	}

	@PostConstruct
	public void init() {
		baseURL = servletContext.getContextPath() + "/";
	}

	@GetMapping(value = "/Details")
	public ModelAndView details(@RequestParam String ISBN, HttpSession session, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		Knjiga knjiga = knjigaService.findOne(ISBN);
		List<Zanr> zanrovi = zanrService.findAll();
		ModelAndView rezultat = new ModelAndView("knjiga");
		Korisnik korisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		List<Komentar> komentariZaPrikaz = new ArrayList<Komentar>();
		List<Komentar> komentari = komentarService.getAll(ISBN);

		for (Komentar komentar : komentari) {
			if (komentar.getStatus().equals(StatusTip.approved)) {
				komentariZaPrikaz.add(komentar);
			}
		}
		rezultat.addObject("knjiga", knjiga);
		rezultat.addObject("zanrovi", zanrovi);
		rezultat.addObject("komentari", komentariZaPrikaz);
		return rezultat;
	}

	@GetMapping(value = "/Edit")
	public ModelAndView edit(@RequestParam String ISBN, HttpSession session, HttpServletResponse response) {

		Knjiga knjiga = knjigaService.findOne(ISBN);

		ModelAndView rezultat = new ModelAndView("izmenaKnjiga");
		rezultat.addObject("knjiga", knjiga);

		return rezultat;
	}

	@PostMapping(value = "/Edit")
	public void edit(@RequestParam String ISBN, @RequestParam String name, @RequestParam String izdavackaKuca,
			@RequestParam String autor, @RequestParam Date releaseYear, @RequestParam String description,
			@RequestParam String picture, @RequestParam double price, @RequestParam int pageCount,
			@RequestParam Boolean isHard, @RequestParam Boolean isLatinica, @RequestParam String language,
			@RequestParam double avgGrade, @RequestParam int brojKnjiga,
			@RequestParam(name = "zanrId", required = false) List<Integer> zanrIds, HttpSession session,
			HttpServletResponse response) throws IOException {

		Knjiga knjiga = knjigaService.findOne(ISBN);
		if (knjiga == null) {
			response.sendRedirect(baseURL + "Knjige");
			return;
		}
		if (name == null || name.equals("") || izdavackaKuca == null || izdavackaKuca.equals("") || autor == null
				|| autor.equals("") || releaseYear == null || releaseYear.equals("") || description == null
				|| description.equals("") || picture == null || picture.equals("") || price <= 0 || pageCount < 0
				|| isHard == null || isLatinica == null || language == null || language.equals("") || avgGrade < 0
				|| price <= 0) {
			response.sendRedirect(baseURL + "Knjige/Details?ISBN=" + ISBN);
			return;
		}

		knjiga.setName(name);
		knjiga.setIzdavackaKuca(izdavackaKuca);
		knjiga.setAutor(autor);
		knjiga.setReleaseYear(releaseYear);
		knjiga.setDescription(description);
		knjiga.setPicture(picture);
		knjiga.setPrice(price);
		knjiga.setPageCount(pageCount);
		knjiga.setHard(isHard);
		knjiga.setLatinica(isLatinica);
		knjiga.setLanguage(language);
		knjiga.setAvgGrade(avgGrade);
		knjiga.setBrojKnjiga(brojKnjiga);

		knjigaService.update(knjiga);

		response.sendRedirect(baseURL + "Knjige");

	}

	@GetMapping
	public ModelAndView index(@RequestParam(required = false) String ISBN, @RequestParam(required = false) String name,
			@RequestParam(required = false) String izdavackaKuca, @RequestParam(required = false) String autor,
			@RequestParam(required = false) Double price, @RequestParam(required = false) Integer pageCount,
			@RequestParam(required = false) String language, @RequestParam(required = false) String sort,
			@RequestParam(required = false) String order, HttpSession session) throws IOException {

		List<Knjiga> knjige = new ArrayList<Knjiga>();
		if (name != null && name.trim().equals(""))
			name = null;

		if (ISBN != null && ISBN.trim().equals(""))
			ISBN = null;

		if (izdavackaKuca != null && izdavackaKuca.trim().equals(""))
			izdavackaKuca = null;

		if (autor != null && autor.trim().equals(""))
			autor = null;

		if (price != null && price.equals(0))
			price = null;

		if (pageCount != null && pageCount.equals(0))
			pageCount = null;

		if (language != null && language.trim().equals(""))
			language = null;

		if (ISBN == null && name == null && izdavackaKuca == null && autor == null && price == null && pageCount == null
				&& language == null) {
			if (sort == null && order == null) {
				knjige = knjigaService.getAll();
			} else {
				knjige = knjigaService.findAllSorted(sort, order);
			}

		} else {
			knjige = knjigaService.find(ISBN, name, izdavackaKuca, autor, price, pageCount, language, sort, order);
		}

		List<Zanr> zanrovi = zanrService.findAll();

		ModelAndView rezultat = new ModelAndView("knjige");
		rezultat.addObject("knjige", knjige);
		rezultat.addObject("zanrovi", zanrovi);
		return rezultat;
	}

	@GetMapping(value = "/Create")
	public ModelAndView create(HttpSession session, HttpServletResponse response) {

		List<Zanr> zanrovi = zanrService.findAll();

		ModelAndView rezultat = new ModelAndView("dodavanjeKnjiga");
		rezultat.addObject("zanrovi", zanrovi);

		return rezultat;
	}

	@PostMapping(value = "/Create")
	public void create(@RequestParam String ISBN, @RequestParam String name, @RequestParam String izdavackaKuca,
			@RequestParam String autor, @RequestParam Date releaseYear, @RequestParam String description,
			@RequestParam String picture, @RequestParam double price, @RequestParam int pageCount,
			@RequestParam Boolean isHard, @RequestParam Boolean isLatinica, @RequestParam String language,
			@RequestParam double avgGrade, @RequestParam int brojKnjiga,
			@RequestParam(name = "zanrId", required = false) List<Integer> zanrIds, HttpSession session,
			HttpServletResponse response) {

		Knjiga knjiga = new Knjiga(ISBN, name, izdavackaKuca, autor, releaseYear, description, picture, price,
				pageCount, isHard, isLatinica, language, avgGrade, brojKnjiga);
		knjiga.setZanr(zanrService.find(zanrIds));
		knjigaService.save(knjiga);

		try {
			response.sendRedirect(baseURL + "Knjige");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@GetMapping(value = "/Delete")
	public ModelAndView delete(HttpSession session, HttpServletResponse response) {

		List<Knjiga> knjige = knjigaService.getAll();

		ModelAndView rezultat = new ModelAndView("brisanjeKnjige");
		rezultat.addObject("knjige", knjige);

		return rezultat;
	}

	@PostMapping(value = "/Delete")
	public void delete(@RequestParam String ISBN, HttpSession session, HttpServletResponse response) {

		knjigaService.delete(ISBN);
		try {
			response.sendRedirect(baseURL + "Knjige");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@GetMapping(value = "/NewComment")
	public ModelAndView newComment(@RequestParam String ISBN, HttpSession session, HttpServletResponse response) {

		ModelAndView rezultat = new ModelAndView("dodavanjeKomentara");
		rezultat.addObject("ISBN", ISBN);
		return rezultat;
	}

	@PostMapping(value = "/NewComment")
	public void create(@RequestParam String text, @RequestParam Integer grade, @RequestParam String ISBN,
			HttpSession session, HttpServletResponse response) {

		Korisnik korisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		StatusTip status = StatusTip.waiting;
		Knjiga knjiga = knjigaService.findOne(ISBN);
		java.util.Date createdOn = new java.util.Date();
		Komentar komentar = new Komentar(null, text, grade, createdOn, korisnik, knjiga, status);
		komentarService.save(komentar);

		List<Komentar> komentari = komentarService.getAll(knjiga.getISBN());

		double sumaOcena = 0;
		for (Komentar koment : komentari) {
			sumaOcena += koment.getGrade();
		}

		double prosecnaOcena = sumaOcena / komentari.size();

		knjiga.setAvgGrade(prosecnaOcena);

		knjigaService.update(knjiga);
		try {
			response.sendRedirect(baseURL + "Knjige");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@GetMapping(value = "/OrderMoreBooks")
	public ModelAndView orderMoreBooks(@RequestParam String ISBN, HttpSession session, HttpServletResponse response) {

		Knjiga knjiga = knjigaService.findOne(ISBN);

		ModelAndView rezultat = new ModelAndView("izmenaKnjiga");
		rezultat.addObject("knjiga", knjiga);

		return rezultat;
	}

	@PostMapping(value = "/OrderMoreBooks")
	public void orderMoreBooks(@RequestParam String ISBN, @RequestParam int brojKnjiga, HttpSession session,
			HttpServletResponse response) throws IOException {

		Knjiga knjiga = knjigaService.findOne(ISBN);
		if (knjiga == null) {
			response.sendRedirect(baseURL + "Knjige");
			return;
		}
		if (brojKnjiga <= 0) {
			response.sendRedirect(baseURL + "Knjige/Details?ISBN=" + ISBN);
			return;
		}
		knjiga.setBrojKnjiga(brojKnjiga);

		knjigaService.update(knjiga);

		response.sendRedirect(baseURL + "Knjige");

	}
}
