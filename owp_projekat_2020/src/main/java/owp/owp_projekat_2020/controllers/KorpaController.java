package owp.owp_projekat_2020.controllers;

import java.io.IOException;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import owp.owp_projekat_2020.models.ElementiKorpe;
import owp.owp_projekat_2020.models.Izvestaj;
import owp.owp_projekat_2020.models.Knjiga;
import owp.owp_projekat_2020.models.Korisnik;
import owp.owp_projekat_2020.models.Korpa;
import owp.owp_projekat_2020.models.LoyaltiKartica;
import owp.owp_projekat_2020.models.SpecialDate;
import owp.owp_projekat_2020.models.Zanr;
import owp.owp_projekat_2020.service.IzvestajService;
import owp.owp_projekat_2020.service.KnjigaService;
import owp.owp_projekat_2020.service.KorpaService;
import owp.owp_projekat_2020.service.LoyaltiKarticaService;
import owp.owp_projekat_2020.service.SpecialDateService;
import owp.owp_projekat_2020.service.impl.LoyaltyKarticaServiceImpl;

@Controller
@RequestMapping(value = "Korpa")
public class KorpaController {

	@Autowired
	private KorpaService korpaService;

	@Autowired
	private KnjigaService knjigaService;

	@Autowired
	private IzvestajService izvestajService;

	@Autowired
	private LoyaltiKarticaService karticaService;

	@Autowired
	private ServletContext servletContext;
	private String baseURL;
	
	@Autowired
	private SpecialDateService specialDateService;

	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	}

	@PostConstruct
	public void init() {
		baseURL = servletContext.getContextPath() + "/";
	}

	@PostMapping(value = "/Kupi")
	public ModelAndView create(@RequestParam String knjigaISBN, HttpSession session, HttpServletResponse response) {
		try {
			Knjiga knjiga = knjigaService.findOne(knjigaISBN);
			Korisnik korisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
			Korpa korpa;
			if (korisnik != null) {
				korpa = korpaService.find(korisnik.getId());
				if (korpa != null) {
					ElementiKorpe element = new ElementiKorpe(null, 1, knjiga, korpa);
					korpaService.saveElementKorpe(element);

				} else {
					Korpa novaKorpa = new Korpa(null, korisnik, true);
					novaKorpa = korpaService.save(novaKorpa);
					ElementiKorpe element = new ElementiKorpe(null, 1, knjiga, novaKorpa);
					korpaService.saveElementKorpe(element);
				}
				response.sendRedirect(baseURL + "Knjige");
				return null;
			}
			response.sendRedirect(baseURL + "Knjige");
			return null;

		} catch (Exception e) {

			ModelAndView rezultat = new ModelAndView("knjige");
			rezultat.addObject("greska", e.getStackTrace());
			return rezultat;

		}
	}

	@GetMapping(value = "/Details")
	public ModelAndView details(HttpSession session, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		try {
			Korisnik korisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
			Korpa korpa;
			ModelAndView rezultat = new ModelAndView("korpa");
			if (korisnik != null) {
				korpa = korpaService.find(korisnik.getId());
				if (korpa != null) {
					ArrayList<ElementiKorpe> elementi = korpaService.findAllElemetns(korpa.getId());
					rezultat.addObject("elementiKorpe", elementi);
				}
				return rezultat;
			}
			return rezultat;
		} catch (Exception e) {
			ModelAndView rezultat = new ModelAndView("korpa");
			rezultat.addObject("greska", e.getStackTrace());
			return rezultat;

		}

	}

	@PostMapping(value = "/Checkout")
	public ModelAndView checkout(@RequestParam int bodovi,HttpSession session, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		ModelAndView rezultat = new ModelAndView("komentar");

		Korisnik korisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		Korpa korpa;
		Date currentDate = new Date();
		SpecialDate specialDate = specialDateService.findDate(currentDate);
		if (korisnik != null) {
			korpa = korpaService.find(korisnik.getId());
			if (korpa != null) {
				ArrayList<ElementiKorpe> elementi = korpaService.findAllElemetns(korpa.getId());
				LoyaltiKartica kartica = karticaService.findOneCard(korisnik);
				double ukupnaCena = 0;
				double cena = 0;
				if(bodovi < 11 && bodovi >= 0 && kartica !=null) {
					if(kartica.getPointCount() < bodovi) {
						response.sendRedirect(baseURL + "Korpa");
					}
				}
				for (ElementiKorpe element : elementi) {
					cena = element.getKnjiga().getPrice() * element.getBrojKupovina();
					Knjiga knjiga = element.getKnjiga();
					knjiga.setBrojKnjiga(knjiga.getBrojKnjiga() - element.getBrojKupovina());
					int procenat = 0;
					if(bodovi != 0 && specialDate == null) {
						procenat = bodovi * 5;
						cena = cena - procenat * cena/100;
						if(kartica != null) {
							kartica.setPointCount(kartica.getPointCount() - bodovi);
							kartica.setDiscount(kartica.getDiscount() * 5);
						}
					}
					
					ukupnaCena = cena + ukupnaCena;
					if(specialDate != null ) {
						ukupnaCena= ukupnaCena - specialDate.getPopust();
					}
					Izvestaj izvestaj = new Izvestaj(0, element.getKnjiga(), new java.util.Date(),
							element.getBrojKupovina(), cena);

					
					knjigaService.update(knjiga);
					izvestajService.save(izvestaj);
				}
				
				if (kartica != null && specialDate == null) {
					while (ukupnaCena > 1000) {
						ukupnaCena = ukupnaCena - 1000;
						kartica.setPointCount(kartica.getPointCount() + 1);
					}
					kartica.setDiscount(kartica.getPointCount() * 5);
					karticaService.update(kartica);

				}

				korpa.setStatus(false);
				korpaService.update(korpa);
			}
		}
		response.sendRedirect(baseURL + "Knjige");
		return rezultat;
	}
	
	@PostMapping(value = "/RemoveElement")
	public void deleteElement(@RequestParam String id, HttpSession session, HttpServletResponse response) {

		korpaService.deleteElement(id);
		try {
			response.sendRedirect(baseURL + "Korpa/Details");
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@PostMapping(value = "/Delete")
	public void delete(@RequestParam String ISBN, HttpSession session, HttpServletResponse response) {

		knjigaService.delete(ISBN);
		try {
			response.sendRedirect(baseURL + "Knjige");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
