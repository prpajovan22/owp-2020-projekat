package owp.owp_projekat_2020.dao;

import java.util.Date;
import java.util.List;

import owp.owp_projekat_2020.models.Izvestaj;
import owp.owp_projekat_2020.models.Knjiga;

public interface IzvestajDAO {

	public int update(Izvestaj izvestaj);

	public void save(Izvestaj izvestaj);
	
	public Izvestaj findOne(Knjiga prodataKnjiga);
	
	public List<Izvestaj> findAll();

	List<Izvestaj> findAllByDateRange(String firstDate,String secondDate);
	
	List<Izvestaj> findAllByDateRangeAndSort(String firstDate,String secondDate,String sort,String order);

}
