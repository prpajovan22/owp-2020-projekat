package owp.owp_projekat_2020.dao;

import java.sql.Date;
import java.util.List;
import owp.owp_projekat_2020.models.Korisnik;

public interface KorisnikDAO {
	
	public Korisnik findOneUser(String username);
	
	public Korisnik findOneUserId(String id);

	public Korisnik findOne(String username, String password);

	public List<Korisnik> findAll();

	public List<Korisnik> find(String username, String password, String email, String firstname, String lastname,
			Date birthDate, String adress, Integer phonenumber, Date registrationDate, Boolean admin,Boolean izblokiran);
	
	public void save(Korisnik korisnik);

}
