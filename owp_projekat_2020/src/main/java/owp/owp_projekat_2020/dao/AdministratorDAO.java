package owp.owp_projekat_2020.dao;

import java.util.List;

import owp.owp_projekat_2020.models.Korisnik;

public interface AdministratorDAO {
	
	public List<Korisnik> findAll();
	
	public int update (Korisnik korisnik);
	
	public Korisnik findOne(String username);

}
