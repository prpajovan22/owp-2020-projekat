package owp.owp_projekat_2020.dao;

import java.util.List;

import owp.owp_projekat_2020.models.Knjiga;

public interface KnjigaDAO {

	List<Knjiga> findAllSorted(String sortColumn, String sortPosition);

	List<Knjiga> findAll();
	
	 Knjiga findOne(String ISBN);
	
	 int save(Knjiga knjiga);
	
	 int update(Knjiga knjiga);
	 
	 int orderMoreBooks(Knjiga knjiga);
	
	 int delete(String ISBN);
	
	 List<Knjiga> sortBy(String sortProperty, String sortOrder);

	List<Knjiga> find(String ISBN, String name, String autor, Double price, String language, String sortColumn, String sortPosition);
}
