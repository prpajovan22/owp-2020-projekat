package owp.owp_projekat_2020.dao;

import java.util.ArrayList;
import java.util.List;

import owp.owp_projekat_2020.models.ElementiKorpe;
import owp.owp_projekat_2020.models.Korpa;

public interface KorpaDAO {

	public Korpa findOneKorisnik(String korisnik);

	public Korpa findOneKorpa(String id);

	public String save(Korpa korpa);

	public int update(Korpa korpa);
	
	public ArrayList<ElementiKorpe> findAllElemetns(String id);
	
	void deleteElement(String id);


}
