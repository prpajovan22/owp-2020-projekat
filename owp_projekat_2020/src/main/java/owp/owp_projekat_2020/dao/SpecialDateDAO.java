package owp.owp_projekat_2020.dao;

import java.util.Date;
import java.util.List;

import owp.owp_projekat_2020.models.Korisnik;
import owp.owp_projekat_2020.models.SpecialDate;

public interface SpecialDateDAO {
	
	public SpecialDate findDate(Date specialDate);
	
	public void save(SpecialDate sDate);
	
	public List<SpecialDate> findAll();

	public List<SpecialDate> find(Date specialDate, int popust);
	
}
