package owp.owp_projekat_2020.dao;

import java.util.List; 

import owp.owp_projekat_2020.models.Komentar;

public interface KomentarDAO {

	public int update(Komentar komentar);

	public void save(Komentar komentar);

	public List<Komentar> findAll(String ISBN);
	
	public Komentar findOne(Integer id);

}
