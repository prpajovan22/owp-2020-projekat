package owp.owp_projekat_2020.dao;


import java.util.List;

import owp.owp_projekat_2020.models.Korisnik;
import owp.owp_projekat_2020.models.LoyaltiKartica;

public interface LoyaltiKarticaDAO {

	public int update (LoyaltiKartica kartica);
	
	public void save(LoyaltiKartica kartica);
	
	public LoyaltiKartica findOneCard(Korisnik korisnik);

	public List<LoyaltiKartica> findAll(int id);
	
	public LoyaltiKartica findById(int id);

	
}
