package owp.owp_projekat_2020.dao;

import java.util.List;

import owp.owp_projekat_2020.models.Zanr;

public interface ZanrDAO {

	public List<Zanr> findAll();
	
	public Zanr findOne(int id);
	
	public int save(Zanr zanr);
	
	public int update(Zanr zanr);
	
	public int delete(int id);
	
}
