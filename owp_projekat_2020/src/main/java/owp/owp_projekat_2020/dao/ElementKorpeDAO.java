package owp.owp_projekat_2020.dao;

import java.util.List;

import owp.owp_projekat_2020.models.ElementiKorpe;
import owp.owp_projekat_2020.models.Korpa;

public interface ElementKorpeDAO {
	 
	public List<ElementiKorpe> findElements(String id);

	public String save(ElementiKorpe elementiKorpe);
	
}
