package owp.owp_projekat_2020.dao.impl;

import java.sql.Date; 
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import owp.owp_projekat_2020.dao.KnjigaDAO;
import owp.owp_projekat_2020.dao.KomentarDAO;
import owp.owp_projekat_2020.dao.KorisnikDAO;
import owp.owp_projekat_2020.models.Knjiga;
import owp.owp_projekat_2020.models.Komentar;
import owp.owp_projekat_2020.models.Korisnik;
import owp.owp_projekat_2020.models.StatusTip;

@Service
public class KomentarDAOImpl implements KomentarDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	private KorisnikDAO korisnikDAO;

	@Autowired
	private KnjigaDAO knjigaDAO;

	private class KomentarRowMapper implements RowMapper<Komentar> {

		private Map<String, Komentar> komentar = new LinkedHashMap<>();

		@Override
		public Komentar mapRow(ResultSet rs, int rowNum) throws SQLException {
			int index = 1;
			Integer id = rs.getInt(index++);
			String text = rs.getString(index++);
			Integer grade = rs.getInt(index++);
			Date date = rs.getDate(index++);
			String korisnikId = rs.getString(index++);
			String ISBN = rs.getString(index++);
			String status = rs.getString(index++);
			StatusTip tip = StatusTip.waiting;
			if (status.equals(StatusTip.approved.toString())) {
				tip = StatusTip.approved;
			} else if (status.equals(StatusTip.not_approved.toString())) {
				tip = StatusTip.not_approved;
			}

			Korisnik korisnisk = korisnikDAO.findOneUserId(korisnikId);

			Knjiga knjiga = knjigaDAO.findOne(ISBN);

			Komentar komentar = new Komentar(id, text, grade, date, korisnisk, knjiga, tip);
			return komentar;
		}

		public List<Komentar> getAll() {
			return new ArrayList<>(komentar.values());
		}
	}

	@Override
	public int update(Komentar komentar) {
		boolean uspeh = true;
		String sql = "UPDATE komentar SET  status = ? WHERE id = ?";
		uspeh = uspeh && jdbcTemplate.update(sql, komentar.getStatus().toString(), komentar.getId()) == 1;

		return uspeh ? 1 : 0;
	}

	@Override
	public void save(Komentar komentar) {
		String sql = "INSERT INTO komentar (text, grade, date, korisnikId,knjigaISBN,status) VALUES (?,?, ?, ?, ?, ?)";
		jdbcTemplate.update(sql, komentar.getText(), komentar.getGrade(), komentar.getDate(), komentar.getAutor().getId(),
				komentar.getKnjiga().getISBN(), komentar.getStatus().toString());
	}

	@Override
	public List<Komentar> findAll(String ISBN) {
		String sql;
		if (ISBN == null) {
			sql = "SELECT id,text, grade, date, korisnikId,knjigaISBN,status" + " FROM komentar ";
			return jdbcTemplate.query(sql, new KomentarRowMapper());

		} else {
			sql = "SELECT id,text, grade, date, korisnikId,knjigaISBN,status" + " FROM komentar Where knjigaISBN = ? ";
			return jdbcTemplate.query(sql, new KomentarRowMapper(), ISBN);

		}
	}

	@Override
	public Komentar findOne(Integer id) {
		try {
			String sql = "SELECT id,text, grade, date, korisnikId,knjigaISBN,status" + " FROM komentar Where id = ? ";
			return jdbcTemplate.queryForObject(sql, new KomentarRowMapper(), id);
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}
}
