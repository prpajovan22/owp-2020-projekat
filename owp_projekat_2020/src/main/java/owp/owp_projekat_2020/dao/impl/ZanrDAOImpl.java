package owp.owp_projekat_2020.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import owp.owp_projekat_2020.dao.ZanrDAO;
import owp.owp_projekat_2020.models.Knjiga;
import owp.owp_projekat_2020.models.Zanr;

@Repository
public class ZanrDAOImpl implements ZanrDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private class ZanrRowCallBackHandler implements RowCallbackHandler {
	private Map<Integer, Zanr> zanrovi = new LinkedHashMap<>();
	
	public void processRow(ResultSet resultSet) throws SQLException {
		int index = 1;
		int zanrId = resultSet.getInt(index++);
		String zanrName = resultSet.getString(index++);
		String zanrDescription = resultSet.getString(index++);
		
		Zanr zanr = zanrovi.get(zanrId);
		if (zanr == null) {
			zanr = new Zanr(zanrId,zanrName,zanrDescription);
			zanrovi.put(zanr.getId(), zanr);
		}
		 
	}
	
	public List<Zanr> getZanrovi() {
		return new ArrayList<>(zanrovi.values());
	}
	}
	
	public Zanr findOne(String ISBN) {
		String sql = "SELECT  id,  name, desciption "
				+ "FROM zanrovi"
				+"WHERE id = ? "
				+"ORDER BY id";
		
		
		ZanrRowCallBackHandler rowCallbackHandler = new ZanrRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler);

		return rowCallbackHandler.getZanrovi().get(0);
	}
	
	@Override
	public List<Zanr> findAll() {
		String sql = "SELECT  id,  name, description FROM zanrovi ORDER BY id";
		
		
		ZanrRowCallBackHandler rowCallbackHandler = new ZanrRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler);

		return rowCallbackHandler.getZanrovi();
	}

	@Override
	public Zanr findOne(int id) {
		String sql = "SELECT  id,  name, description FROM zanrovi WHERE id = ?";
		
		
		ZanrRowCallBackHandler rowCallbackHandler = new ZanrRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler,id);

		return rowCallbackHandler.getZanrovi().get(0);
	}

	@Override
	public int save(Zanr zanr) {
		PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
			
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				String sql = "INSERT INTO knjiga (id,name,description) VALUES (?, ?, ?)";

				PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				int index = 1;
				preparedStatement.setInt(index++, zanr.getId());
				preparedStatement.setString(index++, zanr.getName());
				preparedStatement.setString(index++, zanr.getDescription());

				return preparedStatement;
			}
	};
return 0;
	
}
	

	@Override
	public int update(Zanr zanr) {
		String sql = "DELETE FROM zanrovi WHERE id = ?";
		jdbcTemplate.update(sql, zanr.getId());
		boolean uspeh = true;
		sql = "INSERT INTO knjigaZanr (ISBN, id) VALUES (?, ?)";
		sql = "UPDATE zanrovi SET  name = ?, description = ?, WHERE id = ?";	
		uspeh = uspeh &&  jdbcTemplate.update(sql, zanr.getName(),zanr.getDescription(),zanr.getId()) == 1;
		
		return uspeh?1:0;
	}

	@Override
	public int delete(int id) {
		String sql = "DELETE FROM zanrovi WHERE id = ?";
		jdbcTemplate.update(sql, id);

		sql = "DELETE FROM knjizara WHERE id = ?";
		return jdbcTemplate.update(sql, id);
	}

}
