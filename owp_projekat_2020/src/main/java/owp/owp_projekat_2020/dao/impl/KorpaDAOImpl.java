package owp.owp_projekat_2020.dao.impl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import owp.owp_projekat_2020.dao.KnjigaDAO;
import owp.owp_projekat_2020.dao.KorisnikDAO;
import owp.owp_projekat_2020.dao.KorpaDAO;
import owp.owp_projekat_2020.models.ElementiKorpe;
import owp.owp_projekat_2020.models.Knjiga;
import owp.owp_projekat_2020.models.Korisnik;
import owp.owp_projekat_2020.models.Korpa;
import owp.owp_projekat_2020.models.Zanr;

@Repository
public class KorpaDAOImpl implements KorpaDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	private KorisnikDAO korisnikDAO;

	@Autowired
	private KnjigaDAO knjigaDAO;

	private class KorpaRowMapper implements RowMapper<Korpa> {

		@Override
		public Korpa mapRow(ResultSet rs, int rowNum) throws SQLException {
			int index = 1;
			String id = rs.getString(index++);
			String korisnikId = rs.getString(index++);
			Boolean aktivna = rs.getBoolean(index++);

			Korisnik korinisk = korisnikDAO.findOneUserId(korisnikId);

			Korpa korpa = new Korpa(id, korinisk, aktivna);
			return korpa;
		}

	}

	private class ElementiKorpeRowMapper implements RowCallbackHandler {
		ArrayList<ElementiKorpe> elementi = new ArrayList<ElementiKorpe>();

		@Override
		public void processRow(ResultSet rs) throws SQLException {
			int index = 1;
			String id = rs.getString(index++);
			int brojKupovina = rs.getInt(index++);
			String korpaId = rs.getString(index++);
			String knjigaISBN = rs.getString(index++);

			Korpa korpa = findOneKorpa(korpaId);

			Knjiga knjiga = knjigaDAO.findOne(knjigaISBN);

			ElementiKorpe elementiKorpe = new ElementiKorpe(id, brojKupovina, knjiga, korpa);
			elementi.add(elementiKorpe);

		}

		public List<ElementiKorpe> getElementiKorpe() {
			return elementi;
		}

	}

	@Override
	public Korpa findOneKorisnik(String korisnik) {
		try {
			String sql = "SELECT id,korisnikId,status FROM korpa WHERE korisnikId = ? and status = ?";
			return jdbcTemplate.queryForObject(sql, new KorpaRowMapper(), korisnik, true);
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}

	@Override
	public Korpa findOneKorpa(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String save(Korpa korpa) {
		PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {

			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				String sql = "INSERT INTO korpa (korisnikId,status) VALUES (?, ?)";

				PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				int index = 1;
				preparedStatement.setString(index++, korpa.getKorisnik().getId());
				preparedStatement.setBoolean(index++, korpa.isStatus());

				return preparedStatement;
			}
		};
		GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
		jdbcTemplate.update(preparedStatementCreator, keyHolder);
		return keyHolder.getKey().toString();
	}

	@Override
	public int update(Korpa korpa) {
		String sql = "UPDATE korpa SET  status = ? WHERE id = ?";
		boolean uspeh = jdbcTemplate.update(sql, false, korpa.getId()) == 1;

		return uspeh ? 1 : 0;
	}

	@Override
	public ArrayList<ElementiKorpe> findAllElemetns(String id) {
		String sql = "SELECT * FROM elementiKorpe where korpaId = ?";
		ElementiKorpeRowMapper mapper = new ElementiKorpeRowMapper();
		jdbcTemplate.query(sql, mapper, id);
		return (ArrayList<ElementiKorpe>) mapper.getElementiKorpe();

	}

	@Override
	public void deleteElement(String id) {
		String sql = "DELETE FROM elementiKorpe WHERE id = ?";
		jdbcTemplate.update(sql, id);
	
	}
}
