package owp.owp_projekat_2020.dao.impl;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import owp.owp_projekat_2020.dao.KorisnikDAO;
import owp.owp_projekat_2020.models.Korisnik;

@Repository
public class KorisnikDAOImpl implements KorisnikDAO {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;

	private class KorisnikRowMapper implements RowMapper<Korisnik> {

		@Override
		public Korisnik mapRow(ResultSet rs, int rowNum) throws SQLException {
			int index = 1;
			String id = rs.getString(index++);
			String username = rs.getString(index++);
			String password = rs.getString(index++);
			String email = rs.getString(index++);
			String firstname = rs.getString(index++);
			String lastname = rs.getString(index++);
			Date birthDate = rs.getDate(index++);
			String adress = rs.getString(index++);
			Integer phonenumber = rs.getInt(index++);
			Date registrationDate = rs.getDate(index++);
			Boolean administrator = rs.getBoolean(index++);
			Boolean izblokiran = rs.getBoolean(index++);

			Korisnik korisnik = new Korisnik(id,username, password, email, firstname,lastname, birthDate,adress,phonenumber,registrationDate, administrator,izblokiran);
			return korisnik;
		}

	}
	@Override
	public Korisnik findOneUser(String username) {
		try {
			String sql = "SELECT id,username, password, email, firstname, lastname,birthDate,adress,phonenumber,registrationDate,admin,izblokiran FROM korisnik WHERE username = ?";
			return jdbcTemplate.queryForObject(sql, new KorisnikRowMapper(), username);
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}
	@Override
	public Korisnik findOne(String username, String password) {
		try {
			String sql = "SELECT id ,username, password, email, firstname, lastname,birthDate,adress,phonenumber,registrationDate,admin,izblokiran FROM korisnik WHERE username = ? AND password = ?";
			return jdbcTemplate.queryForObject(sql, new KorisnikRowMapper(), username, password);
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}
	@Override
	public List<Korisnik> findAll() {
		String sql = "SELECT id,username, password, email, firstname, lastname,birthDate,adress,phonenumber,registrationDate,admin,izblokiran"
				+ " FROM korisnik";
		return jdbcTemplate.query(sql, new KorisnikRowMapper());
	}
	
	@Override
	public void save(Korisnik korisnik) {
		String sql = "INSERT INTO korisnik (username, password, email, firstname, lastname,birthDate,adress,phonenumber,registrationDate,admin,izblokiran) VALUES (?,?, ?, ?, ?, ?,?, ?, ?, ?, ?)";
		jdbcTemplate.update(sql, korisnik.getUsername(), korisnik.getPassword(), korisnik.getEmail(), korisnik.getFirstname(),
				korisnik.getLastname(), korisnik.getBirthDate(), korisnik.getAdress(),
				korisnik.getPhonenumber(), korisnik.getRegistrationDate(),korisnik.isAdmin(),korisnik.isIzblokiran());
	}
	@Override
	public List<Korisnik> find(String username, String password, String email, String firstname, String lastname,
			Date birthDate, String adress, Integer phonenumber, Date registrationDate, Boolean admin,Boolean izblokiran) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Korisnik findOneUserId(String id) {
		try {
			String sql = "SELECT id,username, password, email, firstname, lastname,birthDate,adress,phonenumber,registrationDate,admin,izblokiran FROM korisnik WHERE id = ?";
			return jdbcTemplate.queryForObject(sql, new KorisnikRowMapper(), id);
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}

}
