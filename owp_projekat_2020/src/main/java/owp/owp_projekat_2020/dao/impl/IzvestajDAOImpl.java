package owp.owp_projekat_2020.dao.impl;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import owp.owp_projekat_2020.dao.IzvestajDAO;
import owp.owp_projekat_2020.dao.KnjigaDAO;
import owp.owp_projekat_2020.models.Izvestaj;
import owp.owp_projekat_2020.models.Knjiga;

@Repository
public class IzvestajDAOImpl implements IzvestajDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	private KnjigaDAO knjigaDAO;

	private class IzvestajRowMapper implements RowMapper<Izvestaj> {

		private Map<String, Izvestaj> izvestaj = new LinkedHashMap<>();

		@Override
		public Izvestaj mapRow(ResultSet rs, int rowNum) throws SQLException {
			int index = 1;
			Integer id = rs.getInt(index++);
			String ISBN = rs.getString(index++);
			Date datumKupovine = rs.getDate(index++);
			Integer brojProdatijh = rs.getInt(index++);
			Double cenaProdatih = rs.getDouble(index++);

			Knjiga prodataKnjiga = knjigaDAO.findOne(ISBN);

			Izvestaj izvestaj = new Izvestaj(id, prodataKnjiga, datumKupovine, brojProdatijh,cenaProdatih);
			return izvestaj;
		}

	}

	@Override
	public int update(Izvestaj izvestaj) {
		boolean uspeh = true;
		String sql = "UPDATE izvestaj SET  brojProdatijh = ? WHERE id = ?";
		uspeh = uspeh && jdbcTemplate.update(sql, izvestaj.getBrojProdatijh()) == 1;

		return uspeh ? 1 : 0;
	}

	@Override
	public void save(Izvestaj izvestaj) {
		String sql = "INSERT INTO izvestaj (prodataKnjiga,datumKupovine,brojProdatijh,cenaProdatih) VALUES (?,?, ?,?)";
		jdbcTemplate.update(sql, izvestaj.getProdataKnjiga().getISBN(), izvestaj.getDatumKupovine(), izvestaj.getBrojProdatijh(), izvestaj.getCenaProdatih());
	}


	@Override
	public Izvestaj findOne(Knjiga ISBN) {
		try {
			String sql = "SELECT autor, price" + " FROM knjiga Where ISBN = ? ";
			return jdbcTemplate.queryForObject(sql, new IzvestajRowMapper(), ISBN);
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}

	@Override
	public List<Izvestaj> findAll() {
		String sql = "SELECT id,prodataKnjiga,datumKupovine,brojProdatijh,cenaProdatih"
				+ " FROM izvestaj";
		return jdbcTemplate.query(sql, new IzvestajRowMapper());
	}

	@Override
	public List<Izvestaj> findAllByDateRange(String firstDate, String secondDate) {
		String sql = "SELECT id,prodataKnjiga,datumKupovine,brojProdatijh,cenaProdatih FROM izvestaj WHERE datumKupovine BETWEEN '"+firstDate+"' AND '"+secondDate + "'";
		return jdbcTemplate.query(sql, new IzvestajRowMapper());
	}

	@Override
	public List<Izvestaj> findAllByDateRangeAndSort(String firstDate, String secondDate, String sort,
			String order) {
		String sql = "SELECT id,prodataKnjiga,datumKupovine,brojProdatijh,cenaProdatih FROM izvestaj WHERE datumKupovine BETWEEN '"
		+firstDate+"' AND '"+secondDate + "' ORDER BY " + sort + " " + order;
		return jdbcTemplate.query(sql, new IzvestajRowMapper());
	}
}