package owp.owp_projekat_2020.dao.impl;

import java.sql.Connection; 
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import owp.owp_projekat_2020.dao.KnjigaDAO;
import owp.owp_projekat_2020.models.Knjiga;
import owp.owp_projekat_2020.models.Zanr;

@Repository
public class KnjigaDAOImpl implements KnjigaDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	private class KnjigaZanrRowCallBackHandler implements RowCallbackHandler {

		private Map<String, Knjiga> knjige = new LinkedHashMap<>();

		public void processRow(ResultSet resultSet) throws SQLException {
			int index = 1;
			String knjigaISBN = resultSet.getString(index++);
			String knjigaName = resultSet.getString(index++);
			String knjigaKuca = resultSet.getString(index++);
			String knjigaAutor = resultSet.getString(index++);
			Date knjigareleaseYear = resultSet.getDate(index++);
			String knjigaDescription = resultSet.getString(index++);
			String knjigaPicture = resultSet.getString(index++);
			Double knjigaPrice = resultSet.getDouble(index++);
			Integer knjigaPageCount = resultSet.getInt(index++);
			Boolean knjigaHard = resultSet.getBoolean(index++);
			Boolean knjigaLatinica = resultSet.getBoolean(index++);
			String knjigaLanguage = resultSet.getString(index++);
			Double knjigaAvgGrade = resultSet.getDouble(index++);
			Integer knjigaBrojKnjiga = resultSet.getInt(index++);

			Knjiga knjiga = knjige.get(knjigaISBN);
			if (knjiga == null) {
				knjiga = new Knjiga(knjigaISBN, knjigaName, knjigaKuca, knjigaAutor, (java.sql.Date) knjigareleaseYear,
						knjigaDescription, knjigaPicture, knjigaPrice, knjigaPageCount, knjigaHard, knjigaLatinica,
						knjigaLanguage, knjigaAvgGrade, knjigaBrojKnjiga);
				knjige.put(knjiga.getISBN(), knjiga);
			}
			int zanrId = resultSet.getInt(index++);
			String zanrNaziv = resultSet.getString(index++);
			String zanrDescription = resultSet.getString(index++);
			Zanr zanr = new Zanr(zanrId, zanrNaziv, zanrDescription);
			knjiga.getZanr().add(zanr);
		}

		public List<Knjiga> getKnjige() {
			return new ArrayList<>(knjige.values());
		}
	}

	@Override
	public List<Knjiga> findAll() {
		String sql = "SELECT  k.ISBN,  k.name,  k.izdavackaKuca,  k.autor, "
				+ " k.releaseYear, k.description, k.picture, k.price, k.pageCount, k.hard, " + "k.latinica,"
				+ " k.language, k.avgGrade,k.brojKnjiga,z.id,z.name,z.description FROM knjiga k  "
				+ "LEFT JOIN knjigaZanr kz ON kz.ISBN = k.ISBN " + "LEFT JOIN zanrovi z ON kz.zanrId = z.id ";

		KnjigaZanrRowCallBackHandler rowCallbackHandler = new KnjigaZanrRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler);

		return rowCallbackHandler.getKnjige();
	}

	@Override
	public Knjiga findOne(String ISBN) {
		String sql = "SELECT  k.ISBN,  k.name,  k.izdavackaKuca,  k.autor, "
				+ " k.releaseYear, k.description, k.picture, k.price, k.pageCount, k.hard, " + "k.latinica,"
				+ " k.language, k.avgGrade,k.brojKnjiga,z.id,z.name,z.description FROM knjiga k  "
				+ "LEFT JOIN knjigaZanr kz ON kz.ISBN = k.ISBN " + "LEFT JOIN zanrovi z ON kz.zanrId = z.id "
				+ "WHERE k.ISBN = ? ";

		KnjigaZanrRowCallBackHandler rowCallbackHandler = new KnjigaZanrRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler, ISBN);

		return rowCallbackHandler.getKnjige().get(0);
	}

	@Override
	public List<Knjiga> findAllSorted(String sortColumn, String sortPosition) {
		String sql = "SELECT  k.ISBN,  k.name,  k.izdavackaKuca,  k.autor, "
				+ " k.releaseYear, k.description, k.picture, k.price, k.pageCount, k.hard, " + "k.latinica,"
				+ " k.language, k.avgGrade,k.brojKnjiga,z.id,z.name,z.description FROM knjiga k  "
				+ "LEFT JOIN knjigaZanr kz ON kz.ISBN = k.ISBN " + "LEFT JOIN zanrovi z ON kz.zanrId = z.id "
				+ "ORDER BY k." + sortColumn + " " + sortPosition;

		KnjigaZanrRowCallBackHandler rowCallbackHandler = new KnjigaZanrRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler);

		return rowCallbackHandler.getKnjige();

	}

	@Override
	public int save(Knjiga knjiga) {

		PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {

			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				String sql = "INSERT INTO knjiga (ISBN, name, izdavackaKuca, autor, releaseYear,description,picture,price,pageCount,hard,latinica,language,avgGrade,brojKnjiga) VALUES (?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

				PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				int index = 1;
				preparedStatement.setString(index++, knjiga.getISBN());
				preparedStatement.setString(index++, knjiga.getName());
				preparedStatement.setString(index++, knjiga.getIzdavackaKuca());
				preparedStatement.setString(index++, knjiga.getAutor());
				preparedStatement.setDate(index++, knjiga.getReleaseYear());
				preparedStatement.setString(index++, knjiga.getDescription());
				preparedStatement.setString(index++, knjiga.getPicture());
				preparedStatement.setDouble(index++, knjiga.getPrice());
				preparedStatement.setInt(index++, knjiga.getPageCount());
				preparedStatement.setBoolean(index++, knjiga.isHard());
				preparedStatement.setBoolean(index++, knjiga.isLatinica());
				preparedStatement.setString(index++, knjiga.getLanguage());
				preparedStatement.setDouble(index++, knjiga.getAvgGrade());
				preparedStatement.setInt(index++, knjiga.getBrojKnjiga());

				return preparedStatement;
			}

		};

		GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
		boolean uspeh = jdbcTemplate.update(preparedStatementCreator, keyHolder) == 1;
		if (uspeh) {
			String sql = "INSERT INTO knjigaZanr (ISBN, zanrId) VALUES (?, ?)";
			for (Zanr itZanr : knjiga.getZanr()) {
				uspeh = uspeh && jdbcTemplate.update(sql, knjiga.getISBN(), itZanr.getId()) == 1;
			}
		}

		return uspeh ? 1 : 0;
	}

	@Override
	public int update(Knjiga knjiga) {

		String sql = "DELETE FROM knjigaZanr WHERE ISBN = ?";
		jdbcTemplate.update(sql, knjiga.getISBN());
		boolean uspeh = true;
		sql = "INSERT INTO knjigaZanr (ISBN, zanrId) VALUES (?, ?)";
		for (Zanr itZanr : knjiga.getZanr()) {
			if (itZanr.getId() > 0) {
				uspeh = uspeh && jdbcTemplate.update(sql, knjiga.getISBN(), itZanr.getId()) == 1;
			}
		}
		sql = "UPDATE knjiga SET  name = ?, izdavackaKuca = ?, autor = ?, releaseYear = ?, description = ?, picture = ?, price = ?, pageCount = ?, hard = ?, latinica = ?, language = ?, avgGrade = ?, brojKnjiga = ? WHERE ISBN = ?";
		uspeh = uspeh && jdbcTemplate.update(sql, knjiga.getName(), knjiga.getIzdavackaKuca(), knjiga.getAutor(),
				knjiga.getReleaseYear(), knjiga.getDescription(), knjiga.getPicture(), knjiga.getPrice(),
				knjiga.getPageCount(), knjiga.isHard(), knjiga.isLatinica(), knjiga.getLanguage(), knjiga.getAvgGrade(),
				knjiga.getBrojKnjiga(), knjiga.getISBN()) == 1;

		return uspeh ? 1 : 0;
	}

	@Override
	public int delete(String ISBN) {
		String sql = "DELETE FROM knjigaZanr WHERE ISBN = ?";
		jdbcTemplate.update(sql, ISBN);

		sql = "DELETE FROM knjiga WHERE ISBN = ?";
		return jdbcTemplate.update(sql, ISBN);
	}

	@Override
	public List<Knjiga> find(String ISBN, String name, String autor, Double price, String language, String sortColumn,
			String sortPosition) {

		ArrayList<Object> listaArgumenata = new ArrayList<Object>();

		String sql = "SELECT  k.ISBN,  k.name,  k.izdavackaKuca,  k.autor, "
				+ " k.releaseYear, k.description, k.picture, k.price, k.pageCount, k.hard, " + "k.latinica,"
				+ " k.language, k.avgGrade,k.brojKnjiga,z.id,z.name,z.description FROM knjiga k  "
				+ "LEFT JOIN knjigaZanr kz ON kz.ISBN = k.ISBN " + "LEFT JOIN zanrovi z ON kz.zanrId = z.id ";

		StringBuffer whereSql = new StringBuffer(" WHERE ");
		boolean imaArgumenata = false;

		if (name != null) {
			name = "%" + name + "%";
			if (imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("k.name LIKE ?");
			imaArgumenata = true;
			listaArgumenata.add(name);
		}

		if (ISBN != null) {
			if (imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("k.ISBN LIKE ?");
			imaArgumenata = true;
			listaArgumenata.add(ISBN);
		}

		if (price != null) {
			if (imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("k.price = ?");
			imaArgumenata = true;
			listaArgumenata.add(price);
		}

		if (autor != null) {
			if (imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("k.autor LIKE ?");
			imaArgumenata = true;
			listaArgumenata.add(autor);
		}

		sql += whereSql.toString();
		sql += " ORDER BY k." + sortColumn + " " + sortPosition;

		System.out.println(sql);

		List<Knjiga> knjige = jdbcTemplate.query(sql, listaArgumenata.toArray(), new KnjigaRowMapper());

		return knjige;
	}

	private class KnjigaRowMapper implements RowMapper<Knjiga> {

		@Override
		public Knjiga mapRow(ResultSet resultSet, int rowNum) throws SQLException {
			int index = 1;
			String knjigaISBN = resultSet.getString(index++);
			String knjigaName = resultSet.getString(index++);
			String knjigaKuca = resultSet.getString(index++);
			String knjigaAutor = resultSet.getString(index++);
			Date knjigareleaseYear = resultSet.getDate(index++);
			String knjigaDescription = resultSet.getString(index++);
			String knjigaPicture = resultSet.getString(index++);
			Double knjigaPrice = resultSet.getDouble(index++);
			Integer knjigaPageCount = resultSet.getInt(index++);
			Boolean knjigaHard = resultSet.getBoolean(index++);
			Boolean knjigaLatinica = resultSet.getBoolean(index++);
			String knjigaLanguage = resultSet.getString(index++);
			Double knjigaAvgGrade = resultSet.getDouble(index++);
			Integer knjigaBrojKnjiga = resultSet.getInt(index++);

			Knjiga knjiga = new Knjiga(knjigaISBN, knjigaName, knjigaKuca, knjigaAutor,
					(java.sql.Date) knjigareleaseYear, knjigaDescription, knjigaPicture, knjigaPrice, knjigaPageCount,
					knjigaHard, knjigaLatinica, knjigaLanguage, knjigaAvgGrade, knjigaBrojKnjiga);

			int zanrId = resultSet.getInt(index++);
			String zanrNaziv = resultSet.getString(index++);
			String zanrDescription = resultSet.getString(index++);
			Zanr zanr = new Zanr(zanrId, zanrNaziv, zanrDescription);
			knjiga.getZanr().add(zanr);

			return knjiga;

		}

	}

	@Override
	public List<Knjiga> sortBy(String sortProperty, String sortOrder) {

		ArrayList<Object> listaArgumenata = new ArrayList<Object>();

		String sql = "SELECT  k.ISBN,  k.name,  k.izdavackaKuca,  k.autor, "
				+ " k.releaseYear, k.description, k.picture, k.price, k.pageCount, k.hard, " + "k.latinica,"
				+ " k.language, k.avgGrade,k.brojKnjiga,z.id,z.name,z.description FROM knjiga k  "
				+ "LEFT JOIN knjigaZanr kz ON kz.ISBN = k.ISBN " + "LEFT JOIN zanrovi z ON kz.zanrId = z.id ORDER BY "
				+ sortProperty + " " + sortOrder;

		List<Knjiga> knjige = jdbcTemplate.query(sql, listaArgumenata.toArray(), new KnjigaRowMapper());

		return knjige;

	}

	@Override
	public int orderMoreBooks(Knjiga knjiga) {
		
		String sql = "UPDATE knjiga SET brojKnjiga = ? WHERE ISBN = ?";
		jdbcTemplate.update(sql, knjiga.getISBN());
		boolean uspeh = true;
		uspeh = uspeh && jdbcTemplate.update(sql,knjiga.getBrojKnjiga()) == 1;

		return uspeh ? 1 : 0;
	}

}
