package owp.owp_projekat_2020.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Service;

import owp.owp_projekat_2020.dao.ElementKorpeDAO;
import owp.owp_projekat_2020.dao.KnjigaDAO;
import owp.owp_projekat_2020.dao.KorisnikDAO;
import owp.owp_projekat_2020.dao.KorpaDAO;
import owp.owp_projekat_2020.models.ElementiKorpe;
import owp.owp_projekat_2020.models.Knjiga;
import owp.owp_projekat_2020.models.Korisnik;
import owp.owp_projekat_2020.models.Korpa;

@Service
public class ElementiKorpeDAOImpl implements ElementKorpeDAO{

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private class ElementiKorpeRowMapper implements RowMapper<ElementiKorpe> {

		@Override
		public ElementiKorpe mapRow(ResultSet rs, int rowNum) throws SQLException {
			int index = 1;
			String id = rs.getString(index++);
			Integer brojKupovina = rs.getInt(index++);
			Integer korpaId = rs.getInt(index++);
			String knjigaISBN = rs.getString(index++);

			KorpaDAO korpaDAO = new KorpaDAOImpl();
			
			Korpa korpa = korpaDAO.findOneKorpa(id);
			
			KnjigaDAO knjigaDAO = new KnjigaDAOImpl();
			
			Knjiga knjiga = knjigaDAO.findOne(knjigaISBN);
			
			ElementiKorpe elementiKorpe= new ElementiKorpe(id,brojKupovina,knjiga, korpa);
			return elementiKorpe;
		}

	}
	@Override
	public List<ElementiKorpe> findElements(String id) {
		String sql = "SELECT *"
				+ " FROM elementikorpe where korpaId = ?";
		return jdbcTemplate.query(sql, new ElementiKorpeRowMapper(),id);
	}
	@Override
	public String save(ElementiKorpe elementiKorpe) {
		PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
			
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				String sql = "INSERT INTO elementiKorpe (brojKupovina,knjigaISBN,korpaId) VALUES (?, ?, ?)";

				PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				int index = 1;
				preparedStatement.setInt(index++, elementiKorpe.getBrojKupovina());
				preparedStatement.setString(index++, elementiKorpe.getKnjiga().getISBN());
				preparedStatement.setString(index++, elementiKorpe.getKorpa().getId());

				return preparedStatement;
			}
       };
        GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(preparedStatementCreator,keyHolder);
		return keyHolder.getKey().toString();
	}
	
}
