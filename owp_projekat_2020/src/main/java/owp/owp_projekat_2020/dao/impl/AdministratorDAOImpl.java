package owp.owp_projekat_2020.dao.impl;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import owp.owp_projekat_2020.dao.AdministratorDAO;
import owp.owp_projekat_2020.models.Korisnik;
import owp.owp_projekat_2020.models.Zanr;

public class AdministratorDAOImpl implements AdministratorDAO{
	
	@Autowired
	private JdbcTemplate jdbcTemplate;

	private class AdminRowMapper implements RowMapper<Korisnik> {

		@Override
		public Korisnik mapRow(ResultSet rs, int rowNum) throws SQLException {
			int index = 1;
			String id = rs.getString(index++);
			String username = rs.getString(index++);
			String password = rs.getString(index++);
			String email = rs.getString(index++);
			String firstname = rs.getString(index++);
			String lastname = rs.getString(index++);
			Date birthDate = rs.getDate(index++);
			String adress = rs.getString(index++);
			Integer phonenumber = rs.getInt(index++);
			Date registrationDate = rs.getDate(index++);
			Boolean administrator = rs.getBoolean(index++);
			Boolean izblokiran = rs.getBoolean(index++);

			Korisnik korisnik = new Korisnik(id,username, password, email, firstname,lastname, birthDate,adress,phonenumber,registrationDate, true,false);
			return korisnik;
		}

	}

	@Override
	public List<Korisnik> findAll() {
		String sql = "SELECT id,username, password, email, firstname, lastname,birthDate,adress,phonenumber,registrationDate,admin,izblokiran"
				+ " FROM korisnik";
		return jdbcTemplate.query(sql, new AdminRowMapper());
	}

	@Override
	public int update(Korisnik korisnik) {
		boolean uspeh = true;
		String sql = "UPDATE korisnik SET  admin = ?,izblokiran=? WHERE id = ?";	
		uspeh = uspeh &&  jdbcTemplate.update(sql, korisnik.isAdmin(),korisnik.getId()) == 1;
		
		return uspeh?1:0;
	}

	@Override
	public Korisnik findOne(String username) {
		try {
			String sql = "SELECT id ,username, password, email, firstname, lastname,birthDate,adress,phonenumber,registrationDate,admin,izblokiran FROM korisnik WHERE username = ? AND password = ?";
			return jdbcTemplate.queryForObject(sql, new AdminRowMapper(), username);
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}

}
