package owp.owp_projekat_2020.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import owp.owp_projekat_2020.dao.SpecialDateDAO;
import owp.owp_projekat_2020.models.SpecialDate;

@Repository
public class SpecialDateDAOImpl implements SpecialDateDAO{
	
	@Autowired
	private JdbcTemplate jdbcTemplate;

	private class SpecialDateRowMapper implements RowMapper<SpecialDate> {

		@Override
		public SpecialDate mapRow(ResultSet rs, int rowNum) throws SQLException {
			int index = 1;
			String id = rs.getString(index++);
			Date specialDate = rs.getDate(index++);
			int popust = rs.getInt(index++);

			SpecialDate sDate = new SpecialDate(id,specialDate,popust);
			return sDate;
		}

	}

	@Override
	public SpecialDate findDate(Date specialDate) {
		try {
			String sql = "SELECT id,specialDate,popust FROM SpecialDate WHERE specialDate = ?";
			return jdbcTemplate.queryForObject(sql, new SpecialDateRowMapper(), specialDate);
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}

	@Override
	public void save(SpecialDate sDate) {
		String sql = "INSERT INTO SpecialDATE (specialDate,popust) VALUES (?)";
		jdbcTemplate.update(sql,sDate.getSpecialDate(),sDate.getPopust());
	}

	@Override
	public List<SpecialDate> findAll() {
		String sql = "SELECT id,specialDate,popust"
				+ " FROM SpecialDate";
		return jdbcTemplate.query(sql, new SpecialDateRowMapper());
	}

	@Override
	public List<SpecialDate> find(Date specialDate, int popust) {
		// TODO Auto-generated method stub
		return null;
	}

}
