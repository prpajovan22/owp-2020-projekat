package owp.owp_projekat_2020.dao.impl;

import java.sql.ResultSet; 
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import owp.owp_projekat_2020.dao.KorisnikDAO;
import owp.owp_projekat_2020.dao.LoyaltiKarticaDAO;
import owp.owp_projekat_2020.models.Korisnik;
import owp.owp_projekat_2020.models.LoyaltiKartica;

@Repository
public class LoyaltiKarticaDAOImpl implements LoyaltiKarticaDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	private KorisnikDAO korisnikDAO;

	private class LoyaltiKarticaRowMapper implements RowMapper<LoyaltiKartica> {

		private Map<String, LoyaltiKartica> kartica = new LinkedHashMap<>();

		@Override
		public LoyaltiKartica mapRow(ResultSet rs, int rowNum) throws SQLException {
			int index = 1;
			Integer id = rs.getInt(index++);
			Double discount = rs.getDouble(index++);
			Double pointCount = rs.getDouble(index++);
			String korisnikId = rs.getString(index++);
			Boolean statusKartice = rs.getBoolean(index++);

			Korisnik korisnik = korisnikDAO.findOneUserId(korisnikId);

			LoyaltiKartica kartica = new LoyaltiKartica(id, discount, pointCount, korisnik, statusKartice);
			return kartica;
		}

		public List<LoyaltiKartica> getAll() {
			return new ArrayList<>(kartica.values());
		}
	}

	@Override
	public int update(LoyaltiKartica kartica) {
		boolean uspeh = true;
		String sql = "UPDATE kartica SET  status = ? WHERE id = ?";
		uspeh = uspeh && jdbcTemplate.update(sql, kartica.isStatusKartice()) == 1;

		return uspeh ? 1 : 0;
	}

	@Override
	public void save(LoyaltiKartica kartica) {
		String sql = "INSERT INTO kartica (discount, pointCount, korisnikId, statusKartice) VALUES (?, ?, ?, ?)";
		jdbcTemplate.update(sql, kartica.getDiscount(), kartica.getPointCount(), kartica.getKorisnik().getId(),
				kartica.isStatusKartice());
	}

	@Override
	public LoyaltiKartica findOneCard(Korisnik korisnik) {
		try {
			String sql = "SELECT id, discount, pointCount, korisnikId, statusKartice" + " FROM kartica Where korisnikId = ? ";
			return jdbcTemplate.queryForObject(sql, new LoyaltiKarticaRowMapper(), korisnik.getId());
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}

	@Override
	public List<LoyaltiKartica> findAll(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public LoyaltiKartica findById(int id) {
		try {
			String sql = "SELECT id  FROM kartica WHERE id = ?";
			return jdbcTemplate.queryForObject(sql, new LoyaltiKarticaRowMapper(), id);
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}

}
